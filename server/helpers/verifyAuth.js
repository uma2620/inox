
const jwt=require('jsonwebtoken');
const { errorMessage,status}=require('../config/status.config');
 module.exports={
 verifyToken : (req, res, next) => {
  const token = req.headers.authorization.split(" ")[1];

  if (!token) {
    errorMessage.error = 'Token not provided';
    return res.status(status.bad).send(errorMessage);
  }
  try {        

    const decoded =  jwt.verify(token, process.env.secret);
    req.user = {
      email: decoded.email,
      userid: decoded.userid,
      is_admin: decoded.is_admin,
     name: decoded.name,
    };
    next();
  } catch (error) {
    errorMessage.error = 'Authentication Failed';
    console.log(error);
    return res.status(status.unauthorized).send(errorMessage);
  }
}

 }