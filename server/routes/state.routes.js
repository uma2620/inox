const express = require('express');
const router = express.Router();
const stateControl=require('../controllers/state.control');
const verifyAuth=require('../helpers/verifyAuth');


router.post('/',verifyAuth.verifyToken,stateControl.createState);
router.get('/',stateControl.getStates);
router.delete('/:statename',verifyAuth.verifyToken,stateControl.deleteState);
module.exports=router;