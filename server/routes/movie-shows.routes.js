const express = require('express');
const router = express.Router();
const movieShowControl=require('../controllers/movie-shows.control');
const verifyAuth=require('../helpers/verifyAuth');


router.post('/',verifyAuth.verifyToken,movieShowControl.createMovieShows);
router.get('/',movieShowControl.displayMovieShows);
router.put('/:movie_showsId',verifyAuth.verifyToken,movieShowControl.updateMovieShow);
router.get('/:movie_showsId',verifyAuth.verifyToken,movieShowControl.getMovieShow);
router.delete('/:movie_showsId',verifyAuth.verifyToken,movieShowControl.deleteMovieShow);

module.exports=router;