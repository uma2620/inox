const express = require('express');
const router = express.Router();
const showControl=require('../controllers/show.control');
const verifyAuth=require('../helpers/verifyAuth');


router.post('/',verifyAuth.verifyToken,showControl.createShow);
router.get('/',verifyAuth.verifyToken,showControl.getShows);
router.put('/:showId',verifyAuth.verifyToken,showControl.updateShow);
router.get('/:showId',verifyAuth.verifyToken,showControl.getShow);
router.delete('/:showId',verifyAuth.verifyToken,showControl.deleteShow);

module.exports=router;