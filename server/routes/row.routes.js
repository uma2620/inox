const express = require('express');
const router = express.Router();
const rowControl=require('../controllers/row.control');
const verifyAuth=require('../helpers/verifyAuth');


router.post('/',verifyAuth.verifyToken,rowControl.addRow);
router.get('/',verifyAuth.verifyToken,rowControl.getRows);
router.get('/:rowId',verifyAuth.verifyToken,rowControl.getRow);
router.delete('/:rowId',verifyAuth.verifyToken,rowControl.deleteRow);
router.put('/:rowId',verifyAuth.verifyToken,rowControl.updateRow);

module.exports=router;