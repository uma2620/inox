const express = require('express');
const router = express.Router();
const movieControl=require('../controllers/movie.control');
const verifyAuth=require('../helpers/verifyAuth');


router.post('/',verifyAuth.verifyToken,movieControl.createMovie);
router.get('/',verifyAuth.verifyToken,movieControl.getMovies);
router.get('/:movieId',verifyAuth.verifyToken,movieControl.getMovie);
router.put('/:movieId',verifyAuth.verifyToken,movieControl.updateMovie);
router.delete('/:movieId',verifyAuth.verifyToken,movieControl.deleteMovie);

module.exports=router;