const express = require('express');
const router = express.Router();
const bookingControl=require('../controllers/booked-movies.control');
const verifyAuth=require('../helpers/verifyAuth');

router.post('/',verifyAuth.verifyToken,bookingControl.createBooking);
router.get('/',verifyAuth.verifyToken,bookingControl.getBookings);
router.post('/movie/:movie',verifyAuth.verifyToken,bookingControl.getBookingbyMoviename);

router.get('/:bookingId',verifyAuth.verifyToken,bookingControl.getBooking);
router.delete('/:bookingId',verifyAuth.verifyToken,bookingControl.deleteBooking);

module.exports=router;
