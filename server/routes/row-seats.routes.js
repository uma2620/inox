const express = require('express');
const router = express.Router();
const rowSeatsControl=require('../controllers/row-seats.control');
const verifyAuth=require('../helpers/verifyAuth');


router.post('/',verifyAuth.verifyToken,rowSeatsControl.addRowSeats);
router.get('/',verifyAuth.verifyToken,rowSeatsControl.getRowSeats);
router.get('/:row_seatsId',verifyAuth.verifyToken,rowSeatsControl.getRowSeat);
router.put('/:row_seatsId',verifyAuth.verifyToken,rowSeatsControl.updateRowSeat);
router.delete('/:row_seatsId',verifyAuth.verifyToken,rowSeatsControl.deleteRowSeat);

module.exports=router;