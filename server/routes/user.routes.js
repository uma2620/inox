const express = require('express');
const router = express.Router();
const userControl=require('../controllers/user.control');
const verifyAuth=require('../helpers/verifyAuth');


router.post('/',userControl.createUser);
router.get('/',userControl.getUsers);
router.get('/:userId',userControl.getUser);
router.post('/login',userControl.login);
 

module.exports=router;