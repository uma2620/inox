const express = require('express');
const router = express.Router();
const seatControl=require('../controllers/seat.control');
const verifyAuth=require('../helpers/verifyAuth');


router.post('/',verifyAuth.verifyToken,seatControl.addSeat);
router.get('/',verifyAuth.verifyToken,seatControl.getSeats);
router.get('/:seatId',verifyAuth.verifyToken,seatControl.getSeat);
router.delete('/:seatId',verifyAuth.verifyToken,seatControl.deleteSeat);
router.put('/:seatId',verifyAuth.verifyToken,seatControl.updateSeat);

module.exports=router;
