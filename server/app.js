require('./config/db.config');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require(`cors`)

const stateRoute=require('./routes/state.routes');
const userRoute=require('./routes/user.routes');
const showRoute=require('./routes/show.routes');
const movieRoute=require('./routes/movie.routes');
const movieShowsRoute=require('./routes/movie-shows.routes');
const seatRoute=require('./routes/seat.routes');
const rowRoute=require('./routes/row.routes');
const rowSeatsRoute=require('./routes/row-seats.routes');
const bookingRoute=require('./routes/booked-movies.routes');



const port = process.env.PORT;
var app = express();

app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
)  
app.all('*', function(req, res, next) {
  var origin = req.get('origin'); 
  res.header('Access-Control-Allow-Origin', origin);
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});
app.use(cors());
app.use('/api/state/:statename',(request,response,next)=>{
    next();
})
app.param(`statename`, (request, response, next, val) => {
    //console.log(val)
    response.locals.state = val;
    //console.log(res.locals.statename, ` res from `)
    next()
  })
  app.param(`userId`, (request, response, next, val) => {
    //console.log(val)
    response.locals.userId = val;
    //console.log(res.locals.statename, ` res from `)
    next()
  })
app.get('/', (request, response) => {
    response.send("movie ticket management");
})

app.use(cors());
app.use('/api/state/:statename/booking',bookingRoute);
app.use('/api/state/:statename/user',userRoute);
app.use('/api/state/:statename/show',showRoute);
app.use('/api/state/:statename/movie',movieRoute);
app.use('/api/state/:statename/movie-shows',movieShowsRoute);
app.use('/api/state/:statename/seat',seatRoute);
app.use('/api/state/:statename/row',rowRoute);
app.use('/api/state/:statename/row-seats',rowSeatsRoute);
app.use('/api/state/',stateRoute);
 
app.listen(port, () => console.log(`Server started at port : ${port}`))
