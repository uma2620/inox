require(`./config`)
const { Client } = require('pg');
const client = new Client({
    connectionString: process.env.connectionString
});
client.connect()
    .then(() => {
        console.log("database  connected  successfully");
    })
    .catch((error) => {
        console.log("error in database connectivity");
        console.log(error)
    })
module.exports = client;