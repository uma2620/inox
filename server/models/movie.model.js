module.exports = class movie {
    movieName;
    language;
    genre;
    fare;
    actor;
    director;
    musicDirector

    constructor(data) {
        this.movieName=data.movieName;
        this.language=data.language;
        this.genre=data.genre;
        this.fare=data.fare;
        this.actor=data.actor;
        this.director=data.director;
        this.musicDirector=data.musicDirector;
        
    }

}