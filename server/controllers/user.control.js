const client = require('../config/db.config');
const User = require('../models/user.model');
const { successMessage, errorMessage, status } = require('../config/status.config');
const { hashPassword,
    comparePassword,
    isValidEmail,
    validatePassword,
    isEmpty,
    generateUserToken, } = require('../helpers/validations');

module.exports = {
    createUser: (request, response) => {
        state = response.locals.state;
        const user = new User(request.body);
        if (isEmpty(user.email) || isEmpty(user.name) || isEmpty(user.password)) {
            errorMessage.error = 'Email, password, name field cannot be empty';
            return response.status(status.bad).send(errorMessage);
        }
        if (!isValidEmail(user.email)) {
            errorMessage.error = 'Please enter a valid Email';
            return response.status(status.bad).send(errorMessage);
        }
        if (!validatePassword(user.password)) {
            errorMessage.error = 'Password must be more than five(5) characters';
            return response.status(status.bad).send(errorMessage);
        }
        const hashedPassword = hashPassword(user.password);

        client.query(`INSERT INTO ${state}.user(name,email,password) VALUES ($1,$2,$3)`, [user.name, user.email, hashedPassword],
            (error, result) => {
                if (result) {
                    //const dbResponse = result.rows[0];
                    //delete dbResponse.password;
                    //const token = generateUserToken(dbResponse.email, dbResponse.userid, dbResponse.name);
                    //successMessage.data = dbResponse;
                    //successMessage.data.token = token;
                    successMessage.message="user created sucessfully"
                    return response.status(status.created).send(successMessage);
                }


                else if (error.routine === '_bt_check_unique') {
                    errorMessage.error ='Email already exist';
                    response.status(status.conflict).send(errorMessage);
                }
                else {
                    errorMessage.error = 'Operation was not successful';
                    return res.status(status.error).send(errorMessage);
                }
            })
    }
    ,
    getUsers: (request, response) => {
        state = response.locals.state;
        client.query(`SELECT * FROM ${state}.user`, (error, result) => {
            if (error) {
            }
            else {
                successMessage.message = " Users fetched succesfully";
                successMessage.results = result.rows;
                response.status(status.success).send(successMessage);
            }

        })
    },
    getUser: (request, response) => {
        state = response.locals.state;
        userId = request.params.userId;
        client.query(`SELECT * FROM ${state}.user WHERE userId=${userId}`, (error, result) => {

            if (error) {
                console.log(error);
                errorMessage.message = error.message;
                response.status(status.error).send(errorMessage);
            }
            else {
                successMessage.message = "user detail fetched succesfully";
                successMessage.results = result.rows;

                response.status(status.success).send(successMessage);
            }

        })
    },
    login: (request, response) => {
        state = response.locals.state;
        const { email, password } = request.body;
        if (isEmpty(email) || isEmpty(password)) {
            errorMessage.error = 'Email or Password is missing';
            return response.status(status.bad).send(errorMessage);
        }
        if (!isValidEmail(email) || !validatePassword(password)) {
            errorMessage.error = 'Enter a valid Email or Password';
            return response.status(status.bad).send(errorMessage);
        }
        user = `SELECT * FROM ${state}.user WHERE email='${request.body.email}'`
        client.query(user, (error, results) => {

            if (results.rowCount == 0) {
                errorMessage.error = 'Email does not exist';
                return response.status(status.notfound).send(errorMessage);

            }
            else if (results.rowCount == 1) {
                const dbResponse = results.rows[0];
                
                if (!comparePassword(dbResponse.password, password)) {
                    errorMessage.error = 'Password is incorrect';
                    return response.status(status.bad).send(errorMessage);
                }

                const token = generateUserToken(dbResponse.email, dbResponse.userid, dbResponse.name,dbResponse.is_admin);
                delete dbResponse.password;
                // successMessage.data = dbResponse;
                // successMessage.data.token = token;
                return response.status(status.success).json({
                    message:"login sucessfull",
                    token:token

                });
            }
            else {

                console.log(error);
                errorMessage.error = 'Operation was not successful';
                return response.status(status.error).send(errorMessage);
            }
        })
    }

}