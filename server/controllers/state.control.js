const client = require('../config/db.config');
const {successMessage,errorMessage,status}=require('../config/status.config');

module.exports = {
    createState: (request, response) => {
        //console.log(response.locals.state);
        console.log("hiii");
        state = request.body.statename;
        const schema = `CREATE SCHEMA IF NOT EXISTS ${state};`;
        client.query(schema, (error, result) => {
            if (error) {
                console.log(error);
                    errorMessage.message=error.message;                    
                    response.status(status.error).send(errorMessage);
            }
            else {
                console.log("schema created successfully");
                const userTable = `CREATE TABLE ${state}.user
         (userId serial PRIMARY KEY,name text,email text UNIQUE,password text,is_admin numeric DEFAULT 0)`;
                client.query(userTable, (error, result) => {
                    if (error) {
                        console.log("error in creation of user table");
                    }
                    else {
                        //admin@123
                        client.query(`INSERT INTO ${state}.user(name,email,password,is_admin) VALUES ($1,$2,$3,$4)`, ["admin","admin@gmail.com","$2a$10$sDw16XFGkQdBiRb29451.OlCe5lrPkNfAPU7fu5Sw9Cv9SxLD/2Ku",1]
                        ,(error,result)=>{
                            if(error){
                                console.log(error);

                                console.log("error to add a admin");
                            }
                            else{
                                console.log("admin added sucessfully")
                            }

                        })
                        console.log("user table created sucessfully");
                    }
                })

                const movieTable = `CREATE TABLE ${state}.movie(movieId serial PRIMARY KEY,movieName text,
         language text,genre text,fare numeric,actor text,director text,musicDirector text)`;
                client.query(movieTable, (error, result) => {
                    if (error) {
                        console.log("error in creation of movie table");
                    }
                    else {
                        console.log("movie table created sucessfully");
                    }
                })

                const showTable = `CREATE TABLE ${state}.show(showId serial PRIMARY KEY,showName text,showTiming time)`;
                client.query(showTable, (error, result) => {
                    if (error) {
                        console.log("error in creation of show table");
                    }
                    else {
                        console.log("show table created sucessfully");
                    }
                })

                const rowTable = `CREATE TABLE ${state}.row(rowId serial PRIMARY KEY,rowNumber numeric)`;
                client.query(rowTable, (error, result) => {
                    if (error) {
                        console.log("error in creation of row table");
                    }
                    else {
                        console.log("row table created sucessfully");
                    }
                })

                const seatTable = `CREATE TABLE ${state}.seat(seatId serial PRIMARY KEY,seatNumber numeric)`;
                client.query(seatTable, (error, result) => {
                    if (error) {
                        console.log("error in creation of seat table");
                    }
                    else {
                        console.log("seat table created sucessfully");
                    }
                })

                const row_seatsTable = `CREATE TABLE ${state}.row_seats(row_seatsId serial PRIMARY KEY,
            seatId integer references ${state}.seat(seatID),rowID integer references ${state}.row(rowID))`;

                client.query(row_seatsTable, (error, result) => {
                    if (error) {
                        console.log("error in creation of row-seats table");
                        console.log(error)
                    }
                    else {
                        console.log("row-seats table created sucessfully");
                    }
                })

                const movie_showsTable = `CREATE TABLE ${state}.movie_shows(movie_showsId serial PRIMARY KEY,fromdate date,todate date,
            showID integer references ${state}.show(showId),movieId integer references ${state}.movie(movieId))`;

                client.query(movie_showsTable, (error, result) => {
                    if (error) {
                        console.log("error in creation of movie-shows table");
                        console.log(error);
                    }
                    else {
                        console.log("movie-shows table created sucessfully");
                    }
                })

                const booked_moviesTable = `CREATE TABLE ${state}.booked_movies(bookingId serial PRIMARY KEY,showDate date,
            userId integer references ${state}.user(userId),row_seatsId integer references ${state}.row_seats(row_seatsId),
            movie_showId integer references ${state}.movie_shows(movie_showsId))`;

                client.query(booked_moviesTable, (error, result) => {
                    if (error) {
                        console.log("error in creation of booked-movies table");
                        console.log(error);

                    }
                    else {
                        console.log("booked-movies table created sucessfully");
                    }
                })
                
                    successMessage.message="schema and tables created succesfully";
                    response.status(status.created).send(successMessage);
                    

            }



        })
    },

    getStates: (request, response) => {
        client.query(`SELECT s.nspname AS states from pg_catalog.pg_namespace s 
        where nspname not in ('information_schema', 'pg_catalog','public')
        and nspname not like 'pg_toast%' and nspname not like 'pg_temp_%';` ,
            (error, results) => {
                if (error) {
                    console.log(error);
                    errorMessage.message=error.message;                    
                    response.status(status.error).send(errorMessage);
                }
                else {
                    successMessage.message="states fetched succesfully";
                    successMessage.results=results.rows;
                    response.status(status.success).send(successMessage.results);
                    }
            })
    },
    deleteState: (request, response) => {
        state = request.params.statename;
        client.query(`DROP SCHEMA ${state} CASCADE`, (error, results) => {
            if (error) {
                console.log(error);
                errorMessage.message=error.message;                    
                response.status(status.error).send(errorMessage);
            }
            else {
                successMessage.message="state deleted succesfully";
                response.status(status.success).send(successMessage);
                }
        })
    }

}