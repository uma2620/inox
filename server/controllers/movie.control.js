const Movie = require('../models/movie.model');
const client = require('../config/db.config');
const { successMessage, errorMessage, status } = require('../config/status.config');


module.exports = {
    createMovie: (request, response) => {
        state = response.locals.state;
        const movie = new Movie(request.body);
        client.query(`INSERT INTO ${state}.movie(movieName,language,genre,fare,actor,director,musicDirector) 
    VALUES($1,$2,$3,$4,$5,$6,$7)`, [movie.movieName, movie.language, movie.genre, movie.fare, movie.actor,
        movie.director, movie.musicDirector], (error, result) => {
            if (error) {
                console.log(error);
                errorMessage.message = error.message;
                response.status(status.error).send(errorMessage);
            }
            else {
                successMessage.message = "movie created";
                response.status(status.created).send(successMessage);
            }

        })

    },

    getMovies: (request, response) => {
        state = response.locals.state;
        client.query(`SELECT * FROM ${state}.movie ORDER BY movieid`, (error, result) => {
            if (error) {
                console.log(error);
                errorMessage.message = error.message;
                response.status(status.error).send(errorMessage);
            }
            else {
                successMessage.message = "movies fetched succesfully";
                successMessage.results = result.rows;
                response.status(status.success).send(successMessage);
            }

        })

    },
    deleteMovie: (request, response) => {
        state = response.locals.state;
        movieId = request.params.movieId;
        client.query(`DELETE FROM ${state}.movie where movieId=${movieId}`,
            (error, result) => {
                if (error) {
                    console.log(error);
                    errorMessage.message = error.message;
                    response.status(status.error).send(errorMessage);
                }
                else {
                    successMessage.message = "movie deleted succesfully";
                    response.status(status.success).send(successMessage);
                }
            })
    },

    updateMovie: (request, response) => {
        state = response.locals.state;
        movieId = request.params.movieId;
        const movie = new Movie(request.body);
        client.query(`UPDATE ${state}.movie SET movieName=$1,language=$2,genre=$3,fare=$4,actor=$5,
director=$6,musicDirector=$7 WHERE movieId=${movieId}`, [movie.movieName, movie.language, movie.genre,
        movie.fare, movie.actor, movie.director, movie.musicDirector], (error, result) => {
            if (error) {
                console.log(error);
                errorMessage.message = error.message;
                response.status(status.error).send(errorMessage);
            }
            else {
                successMessage.message = "movie updated succesfully";
                response.status(status.success).send(successMessage);
            }
        })
    },

    getMovie: (request, response) => {
        state = response.locals.state;
        movieId = request.params.movieId;
        client.query(`SELECT * FROM ${state}.movie where movieId=${movieId}`, (error, result) => {

            if (error) {
                console.log(error);
                errorMessage.message = error.message;
                response.status(status.error).send(errorMessage);
            }
            else {
                successMessage.message = "movie detail fetched succesfully";
                successMessage.results = result.rows;
                response.status(status.success).send(successMessage);
            }
        })

    }
}
