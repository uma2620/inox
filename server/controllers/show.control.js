const Show = require('../models/show.model');
const client = require('../config/db.config');
const { successMessage, errorMessage, status } = require('../config/status.config');

module.exports = {
    createShow: (request, response) => {
        state = response.locals.state;
        const show = new Show(request.body);
        console.log(request.body.showTiming);
        console.log(request.body.showName);
        client.query(`INSERT INTO ${state}.show(showName,showTiming) VALUES($1,$2)`, [show.showName, show.showTiming],
            (error, result) => {
                if (error) {
                    console.log(error);
                    errorMessage.message = error.message;
                    response.status(status.error).send(errorMessage);
                }
                else {
                    successMessage.message = "show created";
                    response.status(status.created).send(successMessage);
                }
            })
    },
    getShows: (request, response) => {
        state = response.locals.state;
        client.query(`SELECT * FROM ${state}.show`,
            (error, result) => {
                if (error) {
                    console.log(error);
                    errorMessage.message = error.message;
                    response.status(status.error).send(errorMessage);
                }
                else {
                    successMessage.message = "shows fetched succesfully";
                    successMessage.results = result.rows;
                    response.status(status.success).send(successMessage);
                }
            })
    },
    getShow: (request, response) => {
        state = response.locals.state;
        showId = request.params.showId;
        client.query(`SELECT * FROM ${state}.show WHERE showId=${showId}`,
            (error, result) => {
                if (error) {
                    console.log(error);
                    errorMessage.message = error.message;
                    response.status(status.error).send(errorMessage);
                }
                else {
                    successMessage.message = "show detail fetched succesfully";
                    successMessage.results = result.rows;
                    response.status(status.success).send(successMessage);
                }
            })
    },

    deleteShow: (request, response) => {
        state = response.locals.state;
        showId = request.params.showId;
        client.query(`DELETE FROM ${state}.show WHERE showId= ${showId}`,
            (error, result) => {
                if (error) {
                    console.log(error);
                    errorMessage.message = error.message;
                    response.status(status.error).send(errorMessage);
                }
                else {
                    successMessage.message = "show deleted succesfully";
                    response.status(status.success).send(successMessage);
                }
            })

    },

    updateShow: (request, response) => {
        state = response.locals.state;
        showId = request.params.showId;
        const show = new Show(request.body);
        client.query(`UPDATE ${state}.show SET showName=$1, showTiming=$2 where showId=${showId}`,
            [show.showName, show.showTiming],
            (error, result) => {
                if (error) {
                    console.log(error);
                    errorMessage.message = error.message;
                    response.status(status.error).send(errorMessage);
                }
                else {
                    successMessage.message = "show updated succesfully";
                    response.status(status.success).send(successMessage);
                }
            }
        )


    }




}