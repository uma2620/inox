const client = require('../config/db.config');
const Booking = require('../models/booked-movies.model');
const { successMessage, errorMessage, status } = require('../config/status.config');

module.exports = {
    createBooking: (request, response) => {
        state = response.locals.state;
        const booking = new Booking(request.body);
        userId = request.body.userId;
        movieId = request.body.movieId;
        showId = request.body.showId;
        seatId = request.body.seatId;
        rowId = request.body.rowId;
        showDate = request.body.showDate;
        console.log(userId, movieId, showId, seatId, rowId, showDate)
        client.query(`INSERT INTO ${state}.booked_movies(userId,showdate,movie_showId,row_seatsId) VALUES(${userId},'${showDate}',
            (select movie_showsId from ${state}.movie_shows WHERE showId=${showId} AND movieId=${movieId}),
            (SELECT row_seatsId from ${state}.row_seats WHERE rowId=${rowId} AND seatId=${seatId}))`,
            (error, request) => {
                if (error) {
                    console.log(error);
                    errorMessage.message = error.message;
                    response.status(status.error).send(errorMessage);
                }
                else {
                    successMessage.message = "booking added";
                    response.status(status.created).send(successMessage);
                }
            })
    },
    deleteBooking: (request, response) => {
        state = response.locals.state;
        bookingId = request.params.bookingId;
        client.query(`DELETE FROM ${state}.booked_movies where bookingId=${bookingId}`, (error, result) => {

            if (error) {
                console.log(error);
                errorMessage.message = error.message;
                response.status(status.error).send(errorMessage);
            }
            else {
                successMessage.message = "booking called";
                response.status(status.success).send(successMessage);
            }
        })
    },
    getBookings: (request, response) => {
        state = response.locals.state;
        // bookingid,rowid,seatid,showname,booked_movies.userid,name,movie.movieid,moviename,showtiming,rownumber,seatnumber
        bookings = `SELECT * FROM ${state}.booked_movies 
         JOIN ${state}.user        ON ${state}.booked_movies.userId=${state}.user.userId
         JOIN ${state}.movie_shows ON ${state}.booked_movies.movie_showId=${state}.movie_shows.movie_showsId
         JOIN ${state}.movie  ON ${state}.movie_shows.movieId =${state}.movie.movieId
         JOIN ${state}.show ON ${state}.movie_shows.showId = ${state}.show.showId
         JOIN ${state}.row_seats   ON ${state}.booked_movies.row_seatsId=${state}.row_seats.row_seatsId
         JOIN ${state}.row ON ${state}.row_seats.rowId =${state}.row.rowId
         JOIN ${state}.seat ON ${state}.row_seats.seatId = ${state}.seat.seatId`

        //name,movieid,moviename,showtiming,rownumber,seatnumber
        //user.userName,movieId,movie.movieName,show.showDate,show.showTiming
        // JOIN ${state}.movie       ON ${state}.movieId=${state}.movie.movieId 
        // JOIN ${state}.show        ON ${state}.booked_movies.showId=${state}.show.showId 
        // JOIN ${state}.row_seats   ON ${state}.booked_movies.row_seatsId=${state}.row_seats.row_seatsId
        // JOIN ${state}.row         ON ${state}.booked_movies.rowId=${state}.row.rowId
        // JOIN ${state}.seat        ON ${state}.booked_movies.seatId=${state}.seat.seatId`;
        client.query(bookings, (error, results) => {
            if (error) {
                console.log(error);
                errorMessage.message = error.message;
                response.status(status.error).send(errorMessage);
            }
            else {
                successMessage.message = "bookings fetched succesfully";
                successMessage.results = results.rows;
                response.status(status.success).send(successMessage);
            }
        })
    },
    getBookingbyMoviename: (request, response) => {
        state = response.locals.state;
        moviename = request.params.movie;
        showname = request.body.showName;
        showdate = request.body.showDate;
        console.log(moviename, showname, showdate);
        // bookingid,rowid,seatid,showname,booked_movies.userid,name,movie.movieid,moviename,showtiming,rownumber,seatnumber
        bookings = `SELECT * FROM ${state}.booked_movies 
         JOIN ${state}.user        ON ${state}.booked_movies.userId=${state}.user.userId
         JOIN ${state}.movie_shows ON ${state}.booked_movies.movie_showId=${state}.movie_shows.movie_showsId
         JOIN ${state}.movie  ON ${state}.movie_shows.movieId =${state}.movie.movieId
         JOIN ${state}.show ON ${state}.movie_shows.showId = ${state}.show.showId
         JOIN ${state}.row_seats   ON ${state}.booked_movies.row_seatsId=${state}.row_seats.row_seatsId
         JOIN ${state}.row ON ${state}.row_seats.rowId =${state}.row.rowId
         JOIN ${state}.seat ON ${state}.row_seats.seatId = ${state}.seat.seatId where moviename = '${moviename}' AND showname='${showname}' AND showdate= '${showdate}'`

        //name,movieid,moviename,showtiming,rownumber,seatnumber
        //user.userName,movieId,movie.movieName,show.showDate,show.showTiming
        // JOIN ${state}.movie       ON ${state}.movieId=${state}.movie.movieId 
        // JOIN ${state}.show        ON ${state}.booked_movies.showId=${state}.show.showId 
        // JOIN ${state}.row_seats   ON ${state}.booked_movies.row_seatsId=${state}.row_seats.row_seatsId
        // JOIN ${state}.row         ON ${state}.booked_movies.rowId=${state}.row.rowId
        // JOIN ${state}.seat        ON ${state}.booked_movies.seatId=${state}.seat.seatId`;
        client.query(bookings, (error, results) => {
            if (error) {
                console.log(error);
                errorMessage.message = error.message;
                response.status(status.error).send(errorMessage);
            }
            else {
                successMessage.message = "bookings fetched succesfully";
                successMessage.results = results.rows;

                response.status(status.success).send(successMessage);
            }
        })
    },


    getBooking: (request, response) => {
        state = response.locals.state;
        bookingId = request.params.bookingId;
        console.log(state)
        booking = `SELECT bookingid,name,showdate,movie.movieid,moviename,showtiming,rownumber,seatnumber FROM ${state}.booked_movies 
        JOIN ${state}.user        ON ${state}.booked_movies.userId=${state}.user.userId
        JOIN ${state}.movie_shows ON ${state}.booked_movies.movie_showId=${state}.movie_shows.movie_showsId
        JOIN ${state}.movie  ON ${state}.movie_shows.movieId =${state}.movie.movieId
        JOIN ${state}.show ON ${state}.movie_shows.showId = ${state}.show.showId
        JOIN ${state}.row_seats   ON ${state}.booked_movies.row_seatsId=${state}.row_seats.row_seatsId
        JOIN ${state}.row ON ${state}.row_seats.rowId =${state}.row.rowId
        JOIN ${state}.seat ON ${state}.row_seats.seatId = ${state}.seat.seatId
        WHERE bookingId=${bookingId};`;
        client.query(booking, (error, results) => {
            if (error) {
                console.log(error);
                errorMessage.message = error.message;
                response.status(status.error).send(errorMessage);
            }
            else {
                successMessage.message = "booking details fetched succesfully";
                successMessage.results = results.rows;

                response.status(status.success).send(successMessage);
            }
        })
    }

}
