const client = require('../config/db.config');
const {successMessage,errorMessage,status}=require('../config/status.config');

module.exports = {
    addRowSeats: (request, response) => {
        state = response.locals.state;
        rowNumber = request.body.rowNumber;
        seatNumber = request.body.seatNumber;
        client.query(`INSERT INTO ${state}.row_seats(rowId,seatId) values(
                    (select rowId from ${state}.row where rowNumber=${rowNumber}),
                    (select seatId from ${state}.seat where seatNumber=${seatNumber}))`,
            (error, ressult) => {
                if (error) {
                    console.log(error);
                    errorMessage.message=error.message;                    
                    response.status(status.error).send(errorMessage);
                }
                else {
                    successMessage.message="row-seats added";
                    response.status(status.created).send(successMessage);
                    }

            })
    },
    getRowSeats:(request,response)=>{
        state=response.locals.state;
        rowSeats=`SELECT * FROM ${state}.row_seats JOIN ${state}.row ON ${state}.row_seats.rowId = row.rowId 
                     JOIN ${state}.seat ON ${state}.row_seats.seatId = ${state}.seat.seatId`;
                     client.query(rowSeats,(error,results)=>{
                        if (error) {
                            console.log(error);
                            errorMessage.message=error.message;                    
                            response.status(status.error).send(errorMessage);
                        }
                        else {
                            successMessage.message="row-seats fetched succesfully";
                            successMessage.results=results.rows;
            
                            response.status(status.success).send(successMessage);
                            }


                     })
    },
    getRowSeat:(request,response)=>{
        state=response.locals.state;
        row_seatsId=request.params.row_seatsId;
        console.log(row_seatsId)
        rowSeat=`SELECT * FROM ${state}.row_seats JOIN ${state}.row ON ${state}.row_seats.rowId = row.rowId 
        JOIN ${state}.seat ON ${state}.row_seats.seatId = ${state}.seat.seatId WHERE row_seatsid = ${row_seatsId}`;
                     client.query(rowSeat,(error,result)=>{
                        if (error) {
                            console.log(error);
                            errorMessage.message=error.message;                    
                            response.status(status.error).send(errorMessage);
                        }
                        else {
                            successMessage.message="row-seat details fetched succesfully";
                            successMessage.results=results.rows;
            
                            response.status(status.success).send(successMessage);
                            }


                     })
    },
    deleteRowSeat:(request,response)=>{
        state=response.locals.state;
        row_seatsId=request.params.row_seatsId;
        client.query(`DELETE FROM ${state}.row_seats WHERE row_seatsId=${row_seatsId}`,(error,result)=>{
            if (error) {
                console.log(error);
                errorMessage.message=error.message;                    
                response.status(status.error).send(errorMessage);
            }
            else {
                successMessage.message="row-seats deleted succesfully";                    
                response.status(status.success).send(successMessage);
                }


        })
        
    },
    updateRowSeat:(request,response)=>{
        state=response.locals.state;
        row_seatsId=request.params.row_seatsId;
        rowNumber = request.body.rowNumber;
        seatNumber = request.body.seatNumber;
        client.query(`UPDATE ${state}.row_seats SET rowId=(select rowId from ${state}.row where rowNumber=${rowNumber}), seatID=(select seatId from ${state}.seat where seatNumber=${seatNumber}) WHERE row_seatsId=${row_seatsId}`,
             (error,result)=>{
                if (error) {
                    console.log(error);
                    errorMessage.message=error.message;                    
                    response.status(status.error).send(errorMessage);
                }
                else {
                    successMessage.message="row-seats updated succesfully";                    
                    response.status(status.success).send(successMessage);
                    }


            })
    }

}