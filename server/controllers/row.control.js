const client = require('../config/db.config');
const Row = require('../models/row.model');
const { successMessage, errorMessage, status } = require('../config/status.config');

module.exports = {

    addRow: (request, response) => {
        state = response.locals.state;
        const row = new Row(request.body);
        console.log(row);
        console.log(request.body.rowNumber);
        client.query(`INSERT INTO ${state}.row(rowNumber) VALUES($1)`, [row.rowNumber],
            (error, results) => {
                if (error) {
                    console.log(error);
                    errorMessage.message = error.message;
                    response.status(status.error).send(errorMessage);
                }
                else {
                    successMessage.message = "row added";
                    response.status(status.created).send(successMessage);
                }
            })
    },

    getRows: (request, response) => {

        state = response.locals.state;
        client.query(`SELECT * FROM ${state}.row`, (error, results) => {
            if (error) {
                console.log(error);
                errorMessage.message = error.message;
                response.status(status.error).send(errorMessage);
            }
            else {
                successMessage.message = "rows fetched succesfully";
                successMessage.results = results.rows;

                response.status(status.success).send(successMessage);
            }
        })
    },
    getRow: (request, response) => {

        state = response.locals.state;
        rowId = request.params.rowId;
        client.query(`SELECT * FROM ${state}.row WHERE rowId=${rowId}`, (error, results) => {
            if (error) {
                console.log(error);
                errorMessage.message = error.message;
                response.status(status.error).send(errorMessage);
            }
            else {
                successMessage.message = "row details fetched succesfully";
                successMessage.results = results.rows;

                response.status(status.success).send(successMessage);
            }
        })
    },
    deleteRow: (request, response) => {
        state = response.locals.state;
        rowId = request.params.rowId;
        client.query(`DELETE FROM ${state}.row WHERE rowId=${rowId}`, (error, results) => {
            if (error) {
                console.log(error);
                errorMessage.message = error.message;
                response.status(status.error).send(errorMessage);
            }
            else {
                successMessage.message = "row deleted succesfully";
                response.status(status.success).send(successMessage);
            }
        })


    },

    updateRow: (request, response) => {
        state = response.locals.state;
        rowId = request.params.rowId;
        const row = new Row(request.body);
        console
        client.query(`UPDATE ${state}.row SET rowNumber=$1 WHERE rowId=${rowId}`, [row.rowNumber], (error, results) => {
            if (error) {
                console.log(error);
                errorMessage.message = error.message;
                response.status(status.error).send(errorMessage);
            }
            else {
                successMessage.message = "row updated succesfully";
                response.status(status.success).send(successMessage);
            }
        })
    }
}