const client = require('../config/db.config');
const { successMessage, errorMessage, status } = require('../config/status.config');


module.exports = {
    createMovieShows: (request, response) => {
        state = response.locals.state;
        fromDate = request.body.fromDate;
        toDate = request.body.toDate;

        movieName = request.body.movieName;
        showName = request.body.showName;
        
        showQuery=`select showid from ${state}.show where showname='${showName}'`;
        client.query(showQuery,(error,result)=>{
            if(error){
                console.log(error);
                errorMessage.message = error.message;
                response.status(status.error).send(errorMessage);
            }
            else{
                showId=result.rows[0].showid;
                console.log(showId);
                 client.query(`SELECT *  FROM  ${state}.movie_shows where showid=${showId} and 
                      fromdate='${fromDate}' AND todate='${toDate}' `,(error,result)=>{
                          console.log(result.rows);
                         if(result.rowCount==1){
                            errorMessage.message = "this show already added on this dates";
                            response.status(status.error).send(errorMessage);
                         }
                         
                        else if(result.rowCount==0){
                            client.query(`INSERT INTO ${state}.movie_shows(fromdate,todate,showId,movieId) VALUES('${fromDate}','${toDate}',
                            (select showId from ${state}.show where showName='${showName}'),
                            (select movieId from ${state}.movie where movieName='${movieName}'));`,
                                        (error, result) => {
                                            if (error) {
                                                console.log(error);
                                                errorMessage.message = error.message;
                                                response.status(status.error).send(errorMessage);
                                            }
                                            else {
                                                successMessage.message = "movie-show added";
                                                response.status(status.created).send(successMessage);
                                            }
                                        })
                        }
                        else {
                            errorMessage.message = error.message;
                            response.status(status.error).send(errorMessage);
                         }
                      })

            }

        })


        

        
        // client.query(`INSERT INTO ${state}.movie_shows(fromdate,todate,showId,movieId) VALUES('${fromDate}','${toDate}',
        //             (select showId from ${state}.show where showName='${showName}'),
        //             (select movieId from ${state}.movie where movieName='${movieName}'));`,
        //     (error, result) => {
        //         if (error) {
        //             console.log(error);
        //             errorMessage.message=error.message;                    
        //             response.status(status.error).send(errorMessage);
        //         }
        //         else {
        //             successMessage.message="movie-show added";
        //             response.status(status.created).send(successMessage);
        //             }
        //     })
    },
    displayMovieShows: (request, response) => {
        state = response.locals.state;
        movieShows = `SELECT *  FROM  ${state}.movie_shows  JOIN ${state}.show ON ${state}.movie_shows.showId=${state}.show.showId
        JOIN ${state}.movie ON ${state}.movie_shows.movieId=${state}.movie.movieId`;
        client.query(movieShows, (error, results) => {
            if (error) {
                console.log(error);
                errorMessage.message = error.message;
                response.status(status.error).send(errorMessage);
            }
            else {
                successMessage.message = "movie-shows fetched succesfully";
                successMessage.results = results.rows;
                response.status(status.success).send(successMessage);
            }

        })
    },
    deleteMovieShow: (request, response) => {
        state = response.locals.state;
        movie_showsId = request.params.movie_showsId;
        client.query(`DELETE FROM ${state}.movie_shows WHERE movie_showsId=${movie_showsId}`,
            (error, results) => {
                if (error) {
                    console.log(error);
                    errorMessage.message = error.message;
                    response.status(status.error).send(errorMessage);
                }
                else {
                    successMessage.message = "movie-show deleted succesfully";
                    response.status(status.success).send(successMessage);
                }
            })

    },
    updateMovieShow: (request, response) => {
        state = response.locals.state;
        movie_showsId = request.params.movie_showsId;
        fromDate = request.body.fromDate;
        toDate = request.body.toDate;

        movieName = request.body.movieName;
        showName = request.body.showName;
        showId = `select showId from ${state}.show where showName='${showName}'`
        movieId = `select movieId from ${state}.movie where movieName='${movieName}'`

        client.query(`UPDATE ${state}.movie_shows SET fromdate= '${fromDate}', todate='${toDate}'showId=(select showId from ${state}.show where showName='${showName}'),movieId=(select movieId from ${state}.movie where movieName='${movieName}')
         WHERE movie_showsId=${movie_showsId}`, (error, results) => {
            if (error) {
                console.log(error);
                errorMessage.message = error.message;
                response.status(status.error).send(errorMessage);
            }
            else {
                successMessage.message = "movie-show updated succesfully";
                response.status(status.success).send(successMessage);
            }
        })
    },
    getMovieShow: (request, response) => {
        state = response.locals.state;
        movie_showsId = request.params.movie_showsId;

        movieShows = `SELECT *  FROM  ${state}.movie_shows  JOIN ${state}.show ON ${state}.movie_shows.showId=${state}.show.showId
        JOIN ${state}.movie ON ${state}.movie_shows.movieId=${state}.movie.movieId  WHERE movie_showsId=${movie_showsId}`;
        client.query(movieShows, (error, results) => {
            if (error) {
                console.log(error);
                errorMessage.message = error.message;
                response.status(status.error).send(errorMessage);
            }
            else {
                successMessage.message = "movie-shows  details fetched succesfully";
                successMessage.results = results.rows;
                response.status(status.success).send(successMessage);
            }

        })
    }




}
