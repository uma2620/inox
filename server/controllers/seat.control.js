const client = require('../config/db.config');
const Seat = require('../models/seat.model');
const { successMessage, errorMessage, status } = require('../config/status.config');


module.exports = {

    addSeat: (request, response) => {
        state = response.locals.state;
        const seat = new Seat(request.body);
        client.query(`INSERT INTO ${state}.seat(seatNumber) VALUES($1)`, [seat.seatNumber],
            (error, results) => {
                if (error) {
                    console.log(error);
                    errorMessage.message = error.message;
                    response.status(status.error).send(errorMessage);
                }
                else {
                    successMessage.message = "seat added";
                    response.status(status.created).send(successMessage);
                }
            })
    },

    getSeats: (request, response) => {

        state = response.locals.state;
        client.query(`SELECT * FROM ${state}.seat`, (error, results) => {
            if (error) {
                console.log(error);
                errorMessage.message = error.message;
                response.status(status.error).send(errorMessage);
            }
            else {
                successMessage.message = "seats fetched succesfully";
                successMessage.results = results.rows;

                response.status(status.success).send(successMessage);
            }
        })
    },
    getSeat: (request, response) => {

        state = response.locals.state;
        seatId = request.params.seatId;
        client.query(`SELECT * FROM ${state}.seat WHERE seatId=${seatId}`, (error, results) => {
            if (error) {
                console.log(error);
                errorMessage.message = error.message;
                response.status(status.error).send(errorMessage);
            }
            else {
                successMessage.message = "seat details fetched succesfully";
                successMessage.results = results.rows;

                response.status(status.success).send(successMessage);
            }
        })
    },
    deleteSeat: (request, response) => {
        state = response.locals.state;
        seatId = request.params.seatId;
        client.query(`DELETE FROM ${state}.seat WHERE seatId=${seatId}`, (error, results) => {
            if (error) {
                console.log(error);
                errorMessage.message = error.message;
                response.status(status.error).send(errorMessage);
            }
            else {
                successMessage.message = "seat deleted succesfully";
                response.status(status.success).send(successMessage);
            }
        })


    },

    updateSeat: (request, response) => {
        state = response.locals.state;
        seatId = request.params.seatId;
        const seat = new Seat(request.body);
        client.query(`UPDATE ${state}.seat SET seatNumber=$1 WHERE seatId=${seatId}`, [seat.seatNumber], (error, results) => {
            if (error) {
                console.log(error);
                errorMessage.message = error.message;
                response.status(status.error).send(errorMessage);
            }
            else {
                successMessage.message = "seat updated succesfully";
                response.status(status.success).send(successMessage);
            }
        })
    }
}