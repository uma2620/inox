import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddShowComponent } from './add-show/add-show.component';
import { ListShowsComponent } from './list-shows/list-shows.component';
import { EditShowComponent } from './edit-show/edit-show.component';


const routes: Routes = [
  { path: '', component: ListShowsComponent },
  { path: 'add', component: AddShowComponent },
  { path: 'edit/:id', component: EditShowComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShowRoutingModule { }
