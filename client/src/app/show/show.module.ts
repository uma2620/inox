import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthInterceptor } from "../auth/auth-interceptor";
import { FormsModule } from '@angular/forms';

import { ShowRoutingModule } from './show-routing.module';
import { AddShowComponent } from './add-show/add-show.component';
import { ListShowsComponent } from './list-shows/list-shows.component';
import { EditShowComponent } from './edit-show/edit-show.component';


@NgModule({
  declarations: [AddShowComponent, ListShowsComponent, EditShowComponent],
  imports: [
    CommonModule,
    ShowRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
})
export class ShowModule { }
