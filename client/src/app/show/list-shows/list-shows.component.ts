import { Component, OnInit } from '@angular/core';
import { ShowService } from '../shared/show.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-list-shows',
  templateUrl: './list-shows.component.html',
  styleUrls: ['./list-shows.component.css']
})
export class ListShowsComponent implements OnInit {
  state: string;
  shows: any[]=[];
  constructor(private service: ShowService, private route: ActivatedRoute, private router: Router) {  }

  ngOnInit() {
    this.state = this.route.snapshot.paramMap.get('state');
    this.service.getShows(this.state).subscribe(
      response => {
        this.shows = response['results'];
      },
      error => {
        console.log(error)
      }
    );
  }
  delete(id) {
    this.service.deleteShow(this.state, id).subscribe(response => {
      this.router.navigate([`/${this.state}/show`]);
    },
      error => {
        console.log(error)
      });;
  }
  addShow() {
    this.router.navigate([`/${this.state}/show/add`]);
  }
  back() {
    this.router.navigate([`/admin/state`]);
  }
}
