import { Component, OnInit } from '@angular/core';
import { Show } from '../shared/show';
import { ShowService } from '../shared/show.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-show',
  templateUrl: './add-show.component.html',
  styleUrls: ['./add-show.component.css']
})
export class AddShowComponent implements OnInit {
  state: string;
  show = new Show();
  constructor(private service: ShowService, private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.state = this.route.snapshot.paramMap.get('state');

  }
  submit() {
    this.service.addShow(this.show, this.state).subscribe(response => {
      this.router.navigate([`/${this.state}/show`]);
    },
      error => {
        console.log(error)
      });;

  }
  back() {
    this.router.navigate([`/${this.state}/show`]);
  }
}
