import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ShowService } from '../shared/show.service';
import { Show } from '../shared/show';

@Component({
  selector: 'app-edit-show',
  templateUrl: './edit-show.component.html',
  styleUrls: ['./edit-show.component.css']
})
export class EditShowComponent implements OnInit {
  show: Show=new Show();
  state: string;
  constructor(private service: ShowService, private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit() {
    this.state = this.route.snapshot.paramMap.get('state');
    this.route.params.subscribe(params => {
      this.service.getShow(this.state, params['id']).subscribe(data => {
        this.show = data['results'][0];
        this.show.showName = this.show['showname'];
        this.show.showTiming = this.show['showtiming'];
      })
    });
  }
  back() {
    this.router.navigate([`/${this.state}/show`]);
  }
  update() {
    this.route.params.subscribe(params => {
      this.service.updateShow(this.show, params['id'], this.state).subscribe(response => {
        this.router.navigate([`/${this.state}/show`]);
      },
        error => {
          console.log(error);
          this.router.navigate(['']);
        });
    });
  }
}
