import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ShowService {

  constructor(private http: HttpClient) { }

  addShow(show, state) {
    return this.http.post(`${environment.apiUrl}/${state}/show`, show);
  }
  updateShow(show, id, state) {
    return this.http.put(`${environment.apiUrl}/${state}/show/${id}`, show)
  }
  getShows(state) {

    return this.http.get(`${environment.apiUrl}/${state}/show`)
  }
  deleteShow(state, id) {
    return this.http.delete(`${environment.apiUrl}/${state}/show/${id}`)
  }
  getShow(state, id) {
    return this.http.get(`${environment.apiUrl}/${state}/show/${id}`);
  }
}
