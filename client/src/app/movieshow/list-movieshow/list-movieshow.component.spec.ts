import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListMovieshowComponent } from './list-movieshow.component';

describe('ListMovieshowComponent', () => {
  let component: ListMovieshowComponent;
  let fixture: ComponentFixture<ListMovieshowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListMovieshowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListMovieshowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
