import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MovieshowService } from '../shared/movieshow.service';

@Component({
  selector: 'app-list-movieshow',
  templateUrl: './list-movieshow.component.html',
  styleUrls: ['./list-movieshow.component.css']
})
export class ListMovieshowComponent implements OnInit {
  movies:any[]=[];
  state:any;
  constructor(private service: MovieshowService, private route: ActivatedRoute,
    private router: Router) {  }

  ngOnInit() {
    this.state = this.route.snapshot.paramMap.get('state');
    this.service.getMovieShows(this.state).subscribe(
      response => {
        this.movies=response['results']; 
              },
      error=>{
        console.log(error)
      }
    );
  }
  addMovieShow(){
    this.router.navigate([`/${this.state}/movie-show/add`]);
   }
   delete(id){
     this.service.deleteMovieShow(this.state,id).subscribe(response=>{
       alert("sucesfully deleted");
       this.router.navigate([`/${this.state}/movie-show`]);
     },
     error=>{
       alert("can't delete the movie show")
     })
   }
   back(){
    this.router.navigate([`/admin/state`]);
    }
  }
