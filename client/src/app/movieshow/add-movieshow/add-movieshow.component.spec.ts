import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMovieshowComponent } from './add-movieshow.component';

describe('AddMovieshowComponent', () => {
  let component: AddMovieshowComponent;
  let fixture: ComponentFixture<AddMovieshowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMovieshowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMovieshowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
