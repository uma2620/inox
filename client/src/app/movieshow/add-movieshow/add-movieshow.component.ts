import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MovieshowService } from '../shared/movieshow.service';
import { Movieshow } from '../shared/movieshow';

@Component({
  selector: 'app-add-movieshow',
  templateUrl: './add-movieshow.component.html',
  styleUrls: ['./add-movieshow.component.css']
})
export class AddMovieshowComponent implements OnInit {
  shows: any[];
  movies: any[];
  state: any;
  control:boolean=false;
  errorMessage:string;
  movieshow:Movieshow=new Movieshow();
  constructor(private service: MovieshowService, private route: ActivatedRoute,
    private router: Router) {
  }
  ngOnInit() {
    this.state = this.route.snapshot.paramMap.get('state');
    this.service.getShows(this.state).subscribe(
      response => {
        this.shows = response['results'];
      },
      error => {
        console.log(error)
      }
    );
    this.service.getMovies(this.state).subscribe(
      response => {
        this.movies = response['results'];
      },
      error => {
       console.log(error)
      }
    );
  }
  back() {
    this.router.navigate([`/${this.state}/movie-show`]);
  }
  submit() {
    this.service.addMovieShow(this.state, this.movieshow).subscribe(response => {
      this.router.navigate([`/${this.state}/movie-show`]);
    },
      error => {
        this.errorMessage=error['error']['message'];
        this.control=true;
      });
  };
}



