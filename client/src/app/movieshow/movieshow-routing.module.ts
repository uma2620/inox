import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddMovieshowComponent } from './add-movieshow/add-movieshow.component';
import { ListMovieshowComponent } from './list-movieshow/list-movieshow.component';


const routes: Routes = [
  { path: 'add', component: AddMovieshowComponent },
  { path: '', component: ListMovieshowComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MovieshowRoutingModule { }
