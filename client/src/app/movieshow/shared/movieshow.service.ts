import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MovieshowService {
  constructor(private http: HttpClient) { }
  getShows(state) {
    return this.http.get(`${environment.apiUrl}/${state}/show`)
  }
  getMovies(state) {
    return this.http.get(`${environment.apiUrl}/${state}/movie`)
  }
  addMovieShow(state, data) {
    console.log(data);
    return this.http.post(`${environment.apiUrl}/${state}/movie-shows`, data)
  }
  getMovieShows(state) {
    return this.http.get(`${environment.apiUrl}/${state}/movie-shows`)
  }
  deleteMovieShow(state, id) {
    return this.http.delete(`${environment.apiUrl}/${state}/movie-shows/${id}`)
  }
}
