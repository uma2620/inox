import { TestBed } from '@angular/core/testing';

import { MovieshowService } from './movieshow.service';

describe('MovieshowService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MovieshowService = TestBed.get(MovieshowService);
    expect(service).toBeTruthy();
  });
});
