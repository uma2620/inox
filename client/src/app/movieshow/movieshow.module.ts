import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule, MatSelectModule, MatGridListModule
} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthInterceptor } from "../auth/auth-interceptor";
import { DatePipe } from '@angular/common';
import { MovieshowRoutingModule } from './movieshow-routing.module';
import { AddMovieshowComponent } from './add-movieshow/add-movieshow.component';
import { ListMovieshowComponent } from './list-movieshow/list-movieshow.component';

@NgModule({
  declarations: [AddMovieshowComponent, ListMovieshowComponent],
  imports: [
    CommonModule,
    MovieshowRoutingModule,
    FormsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    MatSelectModule,
    MatGridListModule,
    HttpClientModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }, DatePipe
  ],
})
export class MovieshowModule { }
