import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListStateComponent } from './list-state/list-state.component';
import { AddStateComponent } from './add-state/add-state.component';
import { RemoveStateComponent } from './remove-state/remove-state.component';


const routes: Routes = [
  { path: '', component: ListStateComponent },
  { path: 'add', component: AddStateComponent },
  { path: 'remove', component: RemoveStateComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StateRoutingModule { }
