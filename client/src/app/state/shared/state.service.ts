import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from '..//..//../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class StateService {

  constructor(private http: HttpClient) { }
  getStates() {
    return this.http.get(environment.apiUrl);
  }
  addState(state) {
    const data = { statename: state }
    return this.http.post(environment.apiUrl, data);
  }
  deleteState(statename) {
    return this.http.delete(environment.apiUrl + '/' + statename);
  }
}