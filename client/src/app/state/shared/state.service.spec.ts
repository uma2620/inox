import { TestBed } from '@angular/core/testing';

import { StateService } from './state.service';
import { HttpClientTestingModule,HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import {environment} from '../../../environments/environment';

const dummyStateListResponse = {
  success:true,
  message:"states fetched sucessfully",
  results: [
  {id:0,states:"tn"},
  {id:1,states:"bihar"},
  {id:2,states:"andra"}  
  ],
};
const dummyAddStateResponse = {
  success:true,
  message:"state added sucessfully",
};
const dummyRemoveStateResponse = {
  success:true,
  message:"state deleted sucessfully",
};


fdescribe('StateService', () => {
  let service: StateService;
  let httpMock: HttpTestingController;

  beforeEach(() =>{ TestBed.configureTestingModule({
    imports:[HttpClientModule,HttpClientTestingModule],
    providers:[StateService]
  })
  service = TestBed.get(StateService);
  httpMock = TestBed.get(HttpTestingController);
});

  it('should be created', () => {
    const service: StateService = TestBed.get(StateService);
    expect(service).toBeTruthy();
  });
  afterEach(() => {
    httpMock.verify();
  });
  describe('#getStates Test',()=>{

    it('should return list of states', () => {
 
      service.getStates().subscribe((res) => {
 
        expect(res).toEqual(dummyStateListResponse);
      });
      const req = httpMock.expectOne(environment.apiUrl);
      expect(req.request.method).toBe('GET');
      req.flush(dummyStateListResponse);
      })
 });
 describe('#ADD State tests', () => {
  it('Should add a state and return a response', () => {

    service.addState("tn").subscribe((res) => {
      expect(res).toBe(dummyAddStateResponse);
    });

    const req = httpMock.expectOne(environment.apiUrl);
    expect(req.request.method).toBe('POST');
    req.flush(dummyAddStateResponse);

  });
});
describe('#RemoveState tests', () => {
  it('Should remove a state and return a response', () => {
 const statename="tn";
    service.deleteState("tn").subscribe((res) => {
      expect(res).toBe(dummyRemoveStateResponse);
    });

    const req = httpMock.expectOne(environment.apiUrl+ '/' + statename);
    expect(req.request.method).toBe('DELETE');
    req.flush(dummyRemoveStateResponse);

  });
});

});
