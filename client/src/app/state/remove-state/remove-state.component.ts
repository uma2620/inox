import { Component, OnInit } from '@angular/core';
import { StateService } from '../shared/state.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-remove-state',
  templateUrl: './remove-state.component.html',
  styleUrls: ['./remove-state.component.css']
})
export class RemoveStateComponent implements OnInit {

  states: any[];
  selectedState:string="select a state";
  constructor(private stateService: StateService, private router: Router) { }

  ngOnInit() {
    this.stateService.getStates()
      .subscribe((data: any[]) => {
        this.states = data;
      })
  }
  delete() {
    this.stateService.deleteState(this.selectedState)
      .subscribe(response => {
        this.router.navigate([`/admin/state`]);
      },
        error => {
          console.log(error)
        });;
  }
  back() {
    this.router.navigate([`/admin/state`])
  }
}
