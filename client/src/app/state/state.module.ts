import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthInterceptor } from "../auth/auth-interceptor";
import { FormsModule } from '@angular/forms';
import { StateRoutingModule } from './state-routing.module';
import { ListStateComponent } from './list-state/list-state.component';
import { AddStateComponent } from './add-state/add-state.component';
import { RemoveStateComponent } from './remove-state/remove-state.component';


@NgModule({
  declarations: [ListStateComponent, AddStateComponent, RemoveStateComponent],
  imports: [
    CommonModule,
    StateRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
})
export class StateModule { }
