import { async, ComponentFixture,getTestBed, TestBed } from '@angular/core/testing';

import { AddStateComponent } from './add-state.component';
import { StateService } from '../shared/state.service';
import { Router,ActivatedRoute } from '@angular/router';
import { AuthService } from '../../auth/auth.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { of } from 'rxjs';


fdescribe('AddStateComponent', () => {
  let component: AddStateComponent;
  let fixture: ComponentFixture<AddStateComponent>;
  let mockRouter;
  let injector:TestBed;
   let service:StateService;
   let auth:AuthService;
 
   beforeEach(async(() => {
    mockRouter = { navigate: jasmine.createSpy('navigate') };

    TestBed.configureTestingModule({
      declarations: [ AddStateComponent ],
      imports:[HttpClientTestingModule,HttpClientModule,FormsModule],
      providers:[StateService,AuthService,{provide: Router, useValue: mockRouter},
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get: () => "tn", //represents the state.
              },
            },
          },
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddStateComponent);
    component = fixture.componentInstance;
    injector = getTestBed();
    service = injector.get(StateService);
    auth=injector.get(AuthService);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it("should navigate to admin page",()=>{
    component.back();
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/admin/state'],{replaceUrl:true});
  })
});
