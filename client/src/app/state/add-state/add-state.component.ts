import { Component, OnInit } from '@angular/core';
import { StateService } from '../shared/state.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-state',
  templateUrl: './add-state.component.html',
  styleUrls: ['./add-state.component.css']
})
export class AddStateComponent implements OnInit {
  control:boolean=false;
  errorMessage:string;
 stateName:string;
  constructor(private stateservice: StateService, private router: Router) { }

  ngOnInit() {
  }
  addState() {
    this.stateservice.addState(this.stateName).subscribe(response => {
      this.router.navigate([`/admin/state`]);
    },
      error => {
        this.errorMessage="enter a state name";
        this.control=true;
  
        console.log(error)
      });;
  }
  back() {
    this.router.navigate([`/admin/state`]);
  }
}
