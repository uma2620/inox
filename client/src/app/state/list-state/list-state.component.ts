import { Component, OnInit } from '@angular/core';
import { StateService } from '../shared/state.service';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-list-state',
  templateUrl: './list-state.component.html',
  styleUrls: ['./list-state.component.css']
})
export class ListStateComponent implements OnInit {

  control: Boolean;
  selectedState: string = "Select a state";
  states: any[];
  constructor(private stateService: StateService, private router: Router, private authservice: AuthService) { }

  ngOnInit() {
    this.control = true;
    this.stateService.getStates()
      .subscribe((data: any[]) => {
        this.states = data;

      })
  }
  change() {
    if (this.selectedState != "Select a state") {
      this.control = false;
    }
    return this.control;
  }
  add() {
    this.router.navigate([`/admin/state/add`]);

  }
  remove() {
    this.router.navigate([`/admin/state/remove`]);

  }
  listShow() {
    this.router.navigate([`/${this.selectedState}/show`]);

  }
  listMovie() {
    this.router.navigate([`/${this.selectedState}/movie`]);

  }
  listMovieShow() {
    this.router.navigate([`/${this.selectedState}/movie-show`]);

  }
  addSeats() {
    this.router.navigate([`/${this.selectedState}/seats`]);

  }
  logout() {
    this.authservice.logout();
  }
  listBookings() {
    this.router.navigate([`/${this.selectedState}/booking`])
  }
}
