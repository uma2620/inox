import { async, ComponentFixture,getTestBed, TestBed } from '@angular/core/testing';
import { ListStateComponent } from './list-state.component';
import { StateService } from '../shared/state.service';
import { Router,ActivatedRoute } from '@angular/router';
import { AuthService } from '../../auth/auth.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { of } from 'rxjs';


fdescribe('ListStateComponent', () => {
  let component: ListStateComponent;
  let fixture: ComponentFixture<ListStateComponent>;
 let mockRouter;
 let injector:TestBed;
  let service:StateService;
  let auth:AuthService;
  
  beforeEach(async(() => {
    mockRouter = { navigate: jasmine.createSpy('navigate') };

    TestBed.configureTestingModule({
      declarations: [ ListStateComponent ],
      imports:[HttpClientTestingModule,HttpClientModule,FormsModule],
      providers:[StateService,AuthService,{provide: Router, useValue: mockRouter},
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get: () => "tn", //represents the state.
              },
            },
          },
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListStateComponent);
    component = fixture.componentInstance;
    injector = getTestBed();
    service = injector.get(StateService);
    auth=injector.get(AuthService);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it("Should call the get states in state service",()=>{
    const data=[{
    id:0,states:"tn"
    },{
      id:1,states:"andra"
    }]
    spyOn(service,'getStates').and.returnValues(of(data));
    component.ngOnInit();
    expect(component.control).toBeTruthy();
    expect(service.getStates).toHaveBeenCalled();
    expect(component.control)
  })
  it("should call the logout in authservice",()=>{
    spyOn(auth,'logout');
    component.logout();
    expect(auth.logout).toHaveBeenCalled();
  })
  it("should navigate to admin add state page",()=>{
    component.add();
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/admin/state/add'], { replaceUrl: true });
  })
  it("should navigate to admin remove state page",()=>{
    component.remove();
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/admin/state/remove'],{replaceUrl:true})
  })
  it("should navigate to list show page",()=>{
    component.listShow("tn");
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/tn/show'],{replaceUrl:true});
  })
  it("should navigate to list movie page",()=>{
    component.listMovie("tn");
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/tn/movie'],{replaceUrl:true});
  })
  it("should navigate to list movie-shows page",()=>{
    component.listMovieShow("tn");
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/tn/movie-show'],{replaceUrl:true});
  })
  it("should navigate to list booking page",()=>{
    component.listBookings("tn");
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/tn/booking']);
  })
  it("should navigate to add seats page",()=>{
    component.addSeats("tn");
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/tn/seats'],{replaceUrl:true});
  })
  it("shoulc change the control",()=>{
    component.selectedState="Select a state";
    component.change();
    expect(component.control).toBeTruthy();
  })
  it("shoulc change the control",()=>{
    component.selectedState="tn";
    component.change();
    expect(component.control).toBeFalsy();
  })



});
