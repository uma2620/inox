import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BookingService } from '../booking.service';
import { DatePipe } from '@angular/common';
import { Movieshow } from '../movieshow';
import * as Config from '../../shared/config.json';
@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  constructor(private datepipe: DatePipe, private authService: AuthService, private bookingservice: BookingService, private route: ActivatedRoute, private router: Router) { }
  movieShow:Movieshow;
  state: string;
  todayDate: any;
  ngOnInit() {
    this.state = this.route.snapshot.paramMap.get('state');
    this.todayDate = this.datepipe.transform(Date.now(), Config.dateFormat);
    this.movieShow=this.bookingservice.getBookingDetails();
    if(this.movieShow.fromdate<this.todayDate){
      this.movieShow.fromdate=this.todayDate;
    }
     if (this.movieShow.moviename == undefined) {
      this.router.navigate([`/${this.state}/user/home`], { replaceUrl: true });
    }
  }
  select(showDate, numberOfSeats) {
    this.bookingservice.showDate = showDate;
    this.bookingservice.showName = this.movieShow.showname;
    this.bookingservice.numberOfSeats = numberOfSeats;
    this.router.navigate([`/${this.state}/book-movie/seat`]);
  }
  back() {
    this.router.navigate([`/${this.state}/user/home`])
  }
}
