import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BookingService } from '../booking.service';
import * as Config from '../../shared/config.json';

@Component({
  selector: 'app-list-booking',
  templateUrl: './list-booking.component.html',
  styleUrls: ['./list-booking.component.css']
})
export class ListBookingComponent implements OnInit {
  state: string;
  bookings: any[];

  constructor(private bookingservice: BookingService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.state = this.route.snapshot.paramMap.get('state');
    this.bookingservice.getBookings(this.state).subscribe(
      response => {
        this.bookings = response['results'];
        for (let booking of this.bookings) {
          if ((booking['showname'] == 'morning-show') || (booking['showname'] == 'night-show')) {
            booking.extension = Config.extensionValue;
          }
          else {
            booking.extension = Config.anotherExtensionValue;
          }
        }
      },
      error => {
        console.log(error)
      }
    );
  }
  back() {
    this.router.navigate([`/admin/state`])
  }
}
