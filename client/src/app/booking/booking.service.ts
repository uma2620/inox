import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import {Movieshow} from './movieshow';

@Injectable({
  providedIn: 'root'
})
export class BookingService {
  showDate: Date;
  showName: string;
  numberOfSeats: number;
  movieshow:Movieshow=new Movieshow();

  constructor(private http: HttpClient,private router:Router) { }
  createBooking(state, data) {
    return this.http.post(`${environment.apiUrl}/${state}/booking`, data)
  }
  getBookings(state) {
    return this.http.get(`${environment.apiUrl}/${state}/booking`)
  }
  getBookedSeats(state, movie, data) {
    console.log(data);
    return this.http.post(`${environment.apiUrl}/${state}/booking/movie/${movie}`, data)
  }
  setBookingDetails(data,state)
  {
    this.movieshow.moviename=data.moviename;
    this.movieshow.language=data.language;
    this.movieshow.genre=data.genre;
    this.movieshow.fare=data.fare;
    this.movieshow.showname=data.showname;
    this.movieshow.showid=data.showid;
    this.movieshow.movieid=data.movieid;
    this.movieshow.fromdate=data.fromdate;
    this.movieshow.todate=data.todate;
    this.movieshow.extension=data.extension;
    this.movieshow.showtiming=data.showtiming
    this.router.navigate([`/${state}/book-movie/add`]);
  }
  getBookingDetails():Movieshow{
     return this.movieshow;
  }
}
