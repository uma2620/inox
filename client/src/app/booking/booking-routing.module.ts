import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookComponent } from './book/book.component';
import { ListBookingComponent } from './list-booking/list-booking.component';
import { AdminGuard } from '../auth/admin.guard';
import { SeatComponent } from './seat/seat.component';


const routes: Routes = [
  {
    path: 'add', component: BookComponent
  },
  { path: 'seat', component: SeatComponent },
  { path: '', component: ListBookingComponent, canActivate: [AdminGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookingRoutingModule { }
