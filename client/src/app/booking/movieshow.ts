export class Movieshow {
    moviename:string;
    showname:string;
    showtiming:string;
    fromdate:any;
    todate:any;
    fare:number;
    movieid:number;
    showid:number;
    language:string;
    genre:string;
    extension:string;
}
