import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BookingService } from '../booking.service';
import {Movieshow} from '../movieshow';

@Component({
  selector: 'app-seat',
  templateUrl: './seat.component.html',
  styleUrls: ['./seat.component.css']
})
export class SeatComponent implements OnInit {
  control:boolean;
  success:boolean;
  movieshow:Movieshow;
  constructor(private authService: AuthService, private bookingservice: BookingService, private route: ActivatedRoute, private router: Router) { }
  bookedSeats: any[];
  data: any = {
    showDate: Date,
    showName: String
  }
  rowValue: any;
  colValue: any;
  booking: any = {
    userId: Number,
    movieId: Number,
    rowId: Number,
    seatId: Number,
    showDate: Date,
  }
  message:string="";
  rowId: number;
  colId: number;
  showId: number;
  movieId: number;
  state: string;
  showDate: any;
  numberOfSeats: any;
  amount: number;
  seats: any[] = [];
  seatString: string;
  userId: number;
  colString: string;
  col: number;
  time: any;
  selection: Boolean;
  rows: any[] = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
  cols: any[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  showname: string;
  finalBookedSeats: any[]=[];
  ngOnInit() {
    this.state = this.route.snapshot.paramMap.get('state');
    this.control=true;
    this.movieshow=this.bookingservice.getBookingDetails();
    if (this.movieshow.moviename == undefined) {
      this.router.navigate([`/${this.state}/user/home`], { replaceUrl: true });
    }
    this.amount = this.movieshow.fare;
    this.booking.movieId = this.movieshow.movieid;;
    this.booking.showId = this.movieshow.showid;
    this.booking.userId = this.authService.getUserId();
    this.numberOfSeats = this.bookingservice.numberOfSeats;
    this.state = this.route.snapshot.paramMap.get('state');
    this.data.showDate = this.bookingservice.showDate;
    this.data.showName = this.movieshow.showname;
    this.bookingservice.getBookedSeats(this.state,this.movieshow.moviename, this.data).subscribe(
      response => {
        this.bookedSeats = response['results'];
        for (let booking of this.bookedSeats) {
          let row: string;
          row = this.rows[booking['rowid'] - 1];
          this.finalBookedSeats.push(row.concat(booking['seatid']));
        }
        console.log(this.finalBookedSeats);
      },
      error => {
        console.log(error)
      }
    );
  }
  check(event) {
    this.seats.push(event.value);
    this.seatString = this.seats.toString()
  }
  confirm() {
    if (this.seats.length == this.numberOfSeats) {
      this.selection = true;
      this.amount=this.movieshow.fare*this.numberOfSeats;
    }
    else {
      alert("select the seats correctly");
      this.selection = false;
      this.router.navigate([`/${this.state}/user/home`])
    }
  }
  book() {
    for (let seat of this.seats) {
      this.seatString = '';
      this.seatString = seat.toString()
      this.rowValue = this.rows.indexOf(this.seatString[0]);
      this.rowValue = this.rowValue + 1;
      this.colString = this.seatString.slice(1);
      this.col = +this.colString;
      this.colValue = this.cols.indexOf(this.col);
      this.colValue = this.colValue + 1;
      this.booking.rowId = this.rowValue;
      this.booking.seatId = this.colValue;
      this.booking.showDate = this.bookingservice.showDate;
      this.bookingservice.createBooking(this.state, this.booking).subscribe(response => {
        this.control=false;
        this.success=true;
        this.message="succesfully booked";
        
      },
        error => {
          console.log(error);
          this.control=false;
        this.success=false;
        this.message="error happened please try again";

        });;
    };
  }
  onSuccess(){
    this.router.navigate([`/${this.state}/user/bookings`]);
  }
  onError(){
    this.router.navigate([`/${this.state}/user/home`]);
  }
  back() {
    this.router.navigate([`/${this.state}/user/home`])
  }
}
