import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthInterceptor } from "../auth/auth-interceptor";
import { DatePipe } from '@angular/common';

import { BookingRoutingModule } from './booking-routing.module';
import { BookComponent } from './book/book.component';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule, MatSelectModule, MatGridListModule
} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { ListBookingComponent } from './list-booking/list-booking.component';
import { SeatComponent } from './seat/seat.component';


@NgModule({
  declarations: [BookComponent, ListBookingComponent, SeatComponent],
  imports: [
    CommonModule,
    BookingRoutingModule,
    FormsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    MatSelectModule,
    MatGridListModule,
    HttpClientModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    DatePipe
  ],
})
export class BookingModule { }
