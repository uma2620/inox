import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  state: string;
  constructor(private authService: AuthService, private router: Router, private route: ActivatedRoute) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | Observable<boolean> | Promise<boolean> {
    const isAdmin = this.authService.getIsAdmin();

    this.state = localStorage.getItem("state");
    console.log(this.state);

    if (!isAdmin) {

      this.router.navigate([`/${this.state}/user/home`], { replaceUrl: true });
    }
    return isAdmin;
  }


}
