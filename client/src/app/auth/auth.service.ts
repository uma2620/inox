import { Injectable } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  admin: boolean = false;
  Logggedin: boolean = false;
  token: any;
  userId: number;
  state: string;
  userName: String;
  constructor(private route: ActivatedRoute, private router: Router) { }
  login(token, state) {
    localStorage.setItem("token", token);
    this.decodeToken();
    if (this.admin == true) {
      this.router.navigate([`/admin/state`]);

    }
    else {
      this.router.navigate([`/${state}/user/home`]);
    }
  }

  decodeToken() {
    this.token = localStorage.getItem("token");
    if (this.token) {
      var decoded = jwt_decode(this.token);
      this.userId = decoded.userid;
      this.userName = decoded.name;
      this.Logggedin = true;
      if (decoded.is_admin == 1) {
        this.admin = true;
      }
      else {
        this.admin = false;

      }
    }
  }
  logout() {
    this.Logggedin = false;
    this.token = null;
    this.userId = null;
    this.admin = false;
    this.userName = null;
    localStorage.removeItem("token");
    localStorage.removeItem("state");
    this.router.navigate([``], { replaceUrl: true });
  }
  isLoggedIn() {
    this.decodeToken();
    return this.Logggedin;
  }
  getState() {
    return this.state;
  }
  getToken() {
    this.decodeToken();
    return this.token;
  }
  getUserId() {
    this.decodeToken();
    return this.userId;
  }
  getUserName() {
    this.decodeToken();
    return this.userName;
  }
  getIsAdmin() {
    this.decodeToken();
    return this.admin;
  }
}
