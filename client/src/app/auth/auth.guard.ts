import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  ActivatedRoute,

  Router
} from "@angular/router";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AuthService } from "./auth.service";

@Injectable()
export class AuthGuard implements CanActivate {
  state: string;
  constructor(private authService: AuthService, private router: Router, private route: ActivatedRoute) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | Observable<boolean> | Promise<boolean> {
    const isAuth = this.authService.isLoggedIn();
    this.state = localStorage.getItem("state");
    console.log(this.state);
    if (!isAuth) {

      this.router.navigate([`/${this.state}/user/login`], { replaceUrl: true });
    }
    return isAuth;
  }

}
