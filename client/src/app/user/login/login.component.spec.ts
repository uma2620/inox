import { LoginComponent } from './login.component';
import { Component, DebugElement } from "@angular/core";
import {RegisterComponent} from '../register/register.component';
import {HomeComponent} from '../home/home.component';
import { RouterModule, Routes } from '@angular/router';
import {SearchPipe} from '../pipes/search.pipe';
import { By } from "@angular/platform-browser";
import { of } from 'rxjs';
import { throwError } from 'rxjs';

import { async,getTestBed, ComponentFixture, TestBed } from '@angular/core/testing';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule } from '@angular/forms';
import { UserService } from '.././shared/user.service';
import { AuthService } from '../../auth/auth.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import{ Observable} from 'rxjs';
import { NgForm } from '@angular/forms';

export class mockUserService
{
  login(state: any, email: String, password: String):Observable<any[]> {
    return of([{ success:true,message:"login Sucessfull",token:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ0b3B0YWwuY29tIiwiZXhwIjoxNDI2NDIwODAwLCJodHRwOi8vdG9wdGFsLmNvbS9qd3RfY2xhaW1zL2lzX2FkbWluIjp0cnVlLCJjb21wYW55IjoiVG9wdGFsIiwiYXdlc29tZSI6dHJ1ZX0.yRQYnWzskCZUxPwaQupWkiUzKELZ49eM7oWxAQK_ZXw"}]);
}}
fdescribe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let mockRouter;
  let service:UserService;
  let auth:AuthService;
  const data={
    token:"ugugugba12vhjvhv"
  }
  let injector:TestBed;
  // let router = {
  //   navigate: jasmine.createSpy('navigate')
  // }
  const userServiceSpy = jasmine.createSpyObj('UserService', {
    login : of([
      data
   ]),
});


  

  beforeEach(async(() => {
    mockRouter = { navigate: jasmine.createSpy('navigate') };

    TestBed.configureTestingModule({
      declarations: [ LoginComponent,RegisterComponent,HomeComponent,SearchPipe],
      imports:[AngularFontAwesomeModule,FormsModule,HttpClientModule,HttpClientTestingModule,
        RouterTestingModule.withRoutes([
          { path: 'tn/user/register', component: RegisterComponent },
          {path:'tn/user/home',component:HomeComponent}
      ])],
      providers:[AuthService,UserService
        ,
        { provide: Router, useValue: mockRouter },

        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get: () => "tn", // represents the state
              },
            },
          },
        },
  ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    injector = getTestBed();
    service = injector.get(UserService);
    auth=injector.get(AuthService);

    component.ngOnInit(); 

    fixture.detectChanges();
    component.loginForm=new NgForm([],[])
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
it("should show a state value",()=>{
  expect(component.state).toEqual("tn");
})
  // xit(`form should be valid`, async(() => {
  //   fixture.detectChanges();
  //   fixture.whenStable().then(() => {

  //   component.loginForm.controls['emailId'].setValue('umavirat@18');
  //   component.loginForm.controls['password'].setValue('umavirat');

  //   // component.loginForm.controls['email'].setValue('uma@gmail.com');
  //   // component.loginForm.controls['name'].setValue('umavirat');
  //   // component.loginForm.controls['address'].setValue('s/13 , tnhb colony,madurai');
  //   expect(component.loginForm.valid).toBeTruthy();
  //   })
  // }));
//   xit('it should check the email is valid',async()=>{

// component.loginForm.controls['email'].setValue("uma@gmail.com")
// expect(component.loginForm.valid).toBeTruthy();
     
//   })
  it('it should render the title',async()=>{
    const title = fixture.debugElement.query(By.css('h3')).nativeElement;
     expect(title.innerHTML).toBe('Login');
  })
  it('should show a error message',async()=>{
      component.control=true;
      component.errorMessage="Error Occured";
      fixture.detectChanges();
      const err = fixture.debugElement.query(By.css('.errorMessage'));
      expect(err.nativeElement.innerHTML).toContain("Error Occured");
  })
  it('should not show a error message when login sucess',async()=>{
      component.control=false;
      fixture.detectChanges();
      const err = fixture.debugElement.query(By.css('.errorMessage'));
      expect(err).toBeFalsy();
  })
  it('should navigate to register page',async()=>{
    // spyOn(component,'register');
    fixture.detectChanges();
    component.register();
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/tn/user/register'], { replaceUrl: true });

  })
  it('should navigate to Home page',async()=>{
    // spyOn(component,'register');
    fixture.detectChanges();
    component.goHome();
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/tn/user/home'], { replaceUrl: true });

  })
it('should call a login method sucessfully',()=>{
  spyOn(service,'login').withArgs('tn','uma@gmail.com','umavirat').and.returnValues(of({ success:true,message:"login Sucessfull",token:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ0b3B0YWwuY29tIiwiZXhwIjoxNDI2NDIwODAwLCJodHRwOi8vdG9wdGFsLmNvbS9qd3RfY2xhaW1zL2lzX2FkbWluIjp0cnVlLCJjb21wYW55IjoiVG9wdGFsIiwiYXdlc29tZSI6dHJ1ZX0.yRQYnWzskCZUxPwaQupWkiUzKELZ49eM7oWxAQK_ZXw"}));
  spyOn(auth,'login').withArgs("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ0b3B0YWwuY29tIiwiZXhwIjoxNDI2NDIwODAwLCJodHRwOi8vdG9wdGFsLmNvbS9qd3RfY2xhaW1zL2lzX2FkbWluIjp0cnVlLCJjb21wYW55IjoiVG9wdGFsIiwiYXdlc29tZSI6dHJ1ZX0.yRQYnWzskCZUxPwaQupWkiUzKELZ49eM7oWxAQK_ZXw"
  ,"tn");
  const data={
    emailId:"uma@gmail.com",
    password:"umavirat"
  }
  component.submit(data);
  // component.token="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ0b3B0YWwuY29tIiwiZXhwIjoxNDI2NDIwODAwLCJodHRwOi8vdG9wdGFsLmNvbS9qd3RfY2xhaW1zL2lzX2FkbWluIjp0cnVlLCJjb21wYW55IjoiVG9wdGFsIiwiYXdlc29tZSI6dHJ1ZX0.yRQYnWzskCZUxPwaQupWkiUzKELZ49eM7oWxAQK_ZXw";
 expect(service.login).toHaveBeenCalled();
 expect(auth.login).toHaveBeenCalled();
 })
 it('should call a login method and show a error message',()=>{
   const error={
       sucess:false,
       message:"login failed",
       error:[
         {
          error:"email Id Not Registered" 
         }
       ]
   }
  spyOn(service,'login').withArgs('tn','uma@gmail.com','umavirat').and.returnValues(throwError(error));
  const data={
    emailId:"uma@gmail.com",
    password:"umavirat"
  }
  component.submit(data);
 expect(service.login).toHaveBeenCalled();
 expect(component.control).toBeTruthy();
}) 
 

});



