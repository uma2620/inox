import { Component, OnInit,ViewChild } from '@angular/core';
import { UserService } from '.././shared/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';
import { NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  state: string;
  token: any;
  loginControl:boolean=false;
  email: string;
  Password: string;
  // errorMessage: string;

  //  @ViewChild('loginForm',{static: false}) loginForm:NgForm;

  constructor(private userService: UserService, private route: ActivatedRoute, private router: Router, private authservice: AuthService,public translate:TranslateService) {
    translate.setDefaultLang('en');
   }
  ngOnInit() {
    this.state = this.route.snapshot.paramMap.get('state');
  }
  submit( ) {
    // var email = form.emailId;
    // var password = form.password;
    this.userService.login(this.state, this.email, this.Password).subscribe(response => {
      this.token = response['token'];
      this.authservice.login(this.token, this.state)
    }, error => {
      // this.errorMessage=error['error']['error'];
      this.loginControl=true;
    });
  }
  register() {
    this.router.navigate([`/${this.state}/user/register`]);
  }
  goHome() {
    this.router.navigate([`/${this.state}/user/home`]);
  }
}
