import { SearchPipe } from './search.pipe';

fdescribe('SearchPipe', () => {
function setup(){
   const pipe=new SearchPipe();
  const items = [];
  items.push({ id: 1, name: 'uma' });
  items.push({ id: 2, name: 'Virat' });
  items.push({ id: 3, name: 'mahi' });
  items.push({ id: 4, name: 'rohini' });

return {pipe,items}

}
  it('it should create an instance', () => {
    const {pipe}=setup();
    expect(pipe).toBeTruthy();
  });

  it('it should filter correctly', () => {
    const {pipe,items}=setup();

     const filtered = pipe.transform(items,'uma');

    expect(filtered.length).toBe(1);
  });
  
  it('it should return 0', () => {
    const {pipe,items}=setup();

     const filtered = pipe.transform(items,'umaa');

    expect(filtered.length).toBe(0);
  });
  it('should return empty array if no items given', () => {
    const items :any[]=[];
const{pipe}= setup();
    const filtered = pipe.transform(items,  'Hans');

    expect(filtered).toEqual([]);
  });
it('should return a items array when no arguments given',()=>{
  const{pipe,items}=setup();
  const filtered=pipe.transform(items);
  expect(filtered).toEqual(items);
})
it('should return null',()=>{
  const{pipe}=setup();
  const items=false;
  const filtered=pipe.transform(false);
  expect(filtered).toEqual(null);
})
});
