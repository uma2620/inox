import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../shared/user.service';
import { AuthService } from '../././../auth/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css']
})
export class BookingsComponent implements OnInit {

  state: string;
  bookings: any[]=[];
  userName: String;
  rows: any[] = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
  cols: any[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  constructor(private authservice: AuthService, private userservice: UserService, private route: ActivatedRoute, private router: Router,public translate: TranslateService) {
    translate.setDefaultLang('en');
   }

  ngOnInit() {
    this.userName = this.authservice.getUserName();
    this.state = this.route.snapshot.paramMap.get('state');
    this.userservice.getBookings(this.state).subscribe(
      response => {
        this.bookings = response['results'];
        for (let booking of this.bookings) {
          let row: string;
          row = this.rows[booking['rowid'] - 1];
          booking.seat = row.concat(booking['seatid'])
          if ((booking['showname'] == 'morning-show') || (booking['showname'] == 'night-show')) {
            booking.extension = 'AM'
          }
          else {
            booking.extension = 'PM'
          }
        }
      },
      error => {
        console.log(error)
      }
    );
  }
  back() {
    this.router.navigate([`/${this.state}/user/home`])
  }
}
