import { async, ComponentFixture, getTestBed,TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../shared/user.service';
import { AuthService } from '../././../auth/auth.service';
import { of } from 'rxjs';
import { throwError } from 'rxjs';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import {SearchPipe}from '../pipes/search.pipe';

import { BookingsComponent } from './bookings.component';
import { MatExpansionPanelContent } from '@angular/material';

fdescribe('BookingsComponent', () => {
  let component: BookingsComponent;
  let fixture: ComponentFixture<BookingsComponent>;
 let mockRouter;
 let injector:TestBed;
 let service:UserService;
 let auth:AuthService;
  beforeEach(async(() => {
    mockRouter = { navigate: jasmine.createSpy('navigate') };

    TestBed.configureTestingModule({
      declarations: [ BookingsComponent ,SearchPipe],
      imports:[HttpClientModule,HttpClientTestingModule
      ],
      
          providers:[AuthService,UserService,
        { provide: Router, useValue: mockRouter },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get: () => "tn", // represents the state
              },
            },
          },
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingsComponent);
    component = fixture.componentInstance;
    injector = getTestBed();
    service = injector.get(UserService);
    auth=injector.get(AuthService);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should navigate to home page',()=>{
    fixture.detectChanges();
    component.back();
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/tn/user/home']);
  });
  it("should return a username",()=>{
    spyOn(auth,'getUserName').and.returnValue("uma");
    component.ngOnInit();
    expect(component.userName).toBe("uma");
    })
    it("should return the statename",()=>{
      component.ngOnInit();
      expect(component.state).toBe("tn");
    })
    it("should return list of bookings and set extention to 'am'",()=>{
      const dummyBookings={
        success:true,
        message:"bookings fetched sucessfully",
        results:[
        {
         booking_id:1,
         userid:1,
         movieid:1,
         moviename:"theri",
         showid:1,
         showdate:"13/4/2020",
         row_seatsid:1,
         rownumber:1,
         seat_number:1,
         showname:"morning-show",
         rowid:1,
         seatid:1
        },
        {
         booking_id:2,
         userid:2,
         movieid:1,
         moviename:"theri",
         showid:1,
         showdate:"13/4/2020",
         row_seatsid:2,
         rownumber:1,
         seat_number:2,
         showname:"night-show",
         rowid:1,
         seatid:2
        }
       ]
     }
     
      spyOn(service,'getBookings').withArgs('tn').and.returnValues(of(dummyBookings));
      component.ngOnInit();
      expect(service.getBookings).toHaveBeenCalled();
    })
    it("should return list of bookings and set extention to 'pm'",()=>{
      const dummyBookings={
        success:true,
        message:"bookings fetched sucessfully",
        results:[
        {
         booking_id:1,
         userid:1,
         movieid:1,
         moviename:"theri",
         showid:1,
         showdate:"13/4/2020",
         row_seatsid:1,
         rownumber:1,
         seat_number:1,
         showname:"evening-show",
         rowid:1,
         seatid:1
        },
        {
         booking_id:2,
         userid:2,
         movieid:1,
         moviename:"theri",
         showid:1,
         showdate:"13/4/2020",
         row_seatsid:2,
         rownumber:1,
         seat_number:2,
         showname:"afternoon-show",
         rowid:1,
         seatid:2
        }
       ]
     }
     
      spyOn(service,'getBookings').withArgs('tn').and.returnValues(of(dummyBookings));
      component.ngOnInit();
      expect(service.getBookings).toHaveBeenCalled();
    })
    
    it("should retun a error",()=>{
      spyOn(console,'log');
       const error={
         success:false,
         message:"cant,get bookings"
       }
       spyOn(service,'getBookings').withArgs('tn').and.returnValues(throwError(error));
       component.ngOnInit();
       expect(service.getBookings).toHaveBeenCalled();
       expect(console.log).toHaveBeenCalledWith(error);
    })
});
