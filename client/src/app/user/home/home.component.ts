import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../shared/user.service';
import { DatePipe } from '@angular/common';
import { BookingService} from '../../booking/booking.service';
import * as Config from '../../shared/config.json';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  search: any = '';
  movies: any[];
  state: string;
  loggedIn: Boolean = false;
  username: String;
  index=0;
  todayDate:any= this.datePipe.transform(Date.now(),Config.dateFormat);
  constructor(private authservice: AuthService, private router: Router, private route: ActivatedRoute, private service: UserService
                 ,private bookingService:BookingService  , private datePipe: DatePipe) { }
  ngOnInit() {
    this.state = this.route.snapshot.paramMap.get('state');
    this.loggedIn = this.authservice.isLoggedIn();
    this.username = this.authservice.getUserName();
    this.service.getMovieShows(this.state).subscribe(response => {
    this.movies = response['results'];
    for (let movie of this.movies) {
      movie.todate = this.datePipe.transform(movie.todate, Config.dateFormat);
      movie.fromdate = this.datePipe.transform(movie.fromdate, Config.dateFormat);
      movie.index=this.index;
      if (movie.todate < this.todayDate) {
        movie.control = false;
      }
      else {
        movie.control = true;
      }
      if ((movie['showname'] == 'morning-show') || (movie['showname'] == 'night-show')) {
        movie.extension = Config.extensionValue;
      }
      else {
        movie.extension = Config.anotherExtensionValue;
      }
      this.index++;
      console.log(this.movies)
    } 
  },
  error => {
    console.log(error)
  }); 
  }
  
  book(index) {
    this.bookingService.setBookingDetails(this.movies[index],this.state);
  }
  logout() {
    this.authservice.logout();
  }
  back() {
    this.router.navigate([``]);
  }
  login() {
    this.router.navigate([`/${this.state}/user/login`]);
  }
  listbookings() {
    this.router.navigate([`/${this.state}/user/bookings`]);
  }
}
