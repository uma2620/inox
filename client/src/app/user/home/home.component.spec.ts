import { async,getTestBed, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { AuthService } from '../../auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../shared/user.service';
import { DatePipe } from '@angular/common';
import { of } from 'rxjs';
import { throwError } from 'rxjs';
import {FormsModule} from '@angular/forms';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import {SearchPipe}from '../pipes/search.pipe';


fdescribe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let mockRouter;
  let injector:TestBed;
  let service:UserService;
  let auth:AuthService;
  let pipe;
  beforeEach(async(() => {
    mockRouter = { navigate: jasmine.createSpy('navigate') };

    TestBed.configureTestingModule({
      declarations: [ HomeComponent,SearchPipe ],
      imports:[HttpClientModule,HttpClientTestingModule,FormsModule],
      
          providers:[AuthService,UserService,DatePipe,
        { provide: Router, useValue: mockRouter },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get: () => "tn", // represents the state
              },
            },
          },
        },
      ]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    injector = getTestBed();
    service = injector.get(UserService);
    auth=injector.get(AuthService);
    pipe=injector.get(DatePipe);
 
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it("should call logout in authservice",()=>{
    spyOn(auth,'logout');
    component.logout();
    expect(auth.logout).toHaveBeenCalled();
  })
  it("should navigate to back",()=>{
    fixture.detectChanges();
    component.back();
    expect(mockRouter.navigate).toHaveBeenCalledWith([''], { replaceUrl: true });
  })
  it("should navigate to login",()=>{
    fixture.detectChanges();
    component.login();
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/tn/user/login'], { replaceUrl: true });
  })
  it("should navigate to bookings",()=>{
    fixture.detectChanges();
    component.listbookings();
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/tn/user/bookings'], { replaceUrl: true });
  })
  it("should call the get movie-shows and return error",()=>{
    const error={
      sucess:false,
      message:"error in fetching movie shows",
      }
  spyOn(service,'getMovieShows').withArgs("tn").and.returnValues(throwError(error));
 spyOn(console,'log');
  const data={
   emailId:"uma@gmail.com",
   password:"umavirat"
  }
 component.ngOnInit();
   expect(service.getMovieShows).toHaveBeenCalled();
   expect(console.log).toHaveBeenCalledWith(error);
  })
  it("should call getMovieshows and return results",()=>{
   spyOn(auth,'getUserName').and.returnValue("uma");
   spyOn(auth,'isLoggedIn').and.returnValue(true);
   const data={
     success:true,
     message:"movie-shows fetched sucessfully",
     results:[
      {
        movie_showsid:1,
        movieid:1,
        moviename:"theri",
        actor:"vijay",
        musicdirector:"gvp",
        director:"atlee",
        language:"tamil",
        genre:"action",
        showname:"morning-show",
        showid:1,
        showtiming:"10:30:00",
        fromdate:"13/4/2020",
        todate:"2020-04-20T12:00:00Z"    
       }
      //  {
      //    movie_showsid:2,
      //    movieid:1,
      //    moviename:"theri",
      //    actor:"vijay",
      //    musicdirector:"gvp",
      //    director:"atlee",
      //    language:"tamil",
      //    genre:"action",
      //    showname:"evening-show",
      //    showid:3,
      //    showtiming:"11:30:00",
      //    fromdate:"20/3/2020",
      //    todate:"20/4/2020"    
      //   }
     ]
   }
   let pipe1=new DatePipe('en');
spyOn(service,'getMovieShows').withArgs("tn").and.returnValues(of(data));
spyOn(pipe1,"transform").withArgs("2020-04-20T12:00:00Z","yyyy-MM-DD").and.returnValue("2020-04-20")
component.ngOnInit();
expect(service.getMovieShows).toHaveBeenCalled();
expect(component.control).toBeTruthy();
expect(component.username).toBe("uma");
expect(component.state).toBe("tn");
})
it("should call getMovieshows and return results",()=>{
  spyOn(auth,'getUserName').and.returnValue("uma");
  spyOn(auth,'isLoggedIn').and.returnValue(true);
  const data={
    success:true,
    message:"movie-shows fetched sucessfully",
    results:[
     {
       movie_showsid:1,
       movieid:1,
       moviename:"theri",
       actor:"vijay",
       musicdirector:"gvp",
       director:"atlee",
       language:"tamil",
       genre:"action",
       showname:"evening-show",
       showid:1,
       showtiming:"10:30:00",
       fromdate:"13/3/2020",
       todate:"2020-03-20T12:00:00Z"    
      }
     //  {
     //    movie_showsid:2,
     //    movieid:1,
     //    moviename:"theri",
     //    actor:"vijay",
     //    musicdirector:"gvp",
     //    director:"atlee",
     //    language:"tamil",
     //    genre:"action",
     //    showname:"evening-show",
     //    showid:3,
     //    showtiming:"11:30:00",
     //    fromdate:"20/3/2020",
     //    todate:"20/4/2020"    
     //   }
    ]
  }
  let pipe1=new DatePipe('en');
spyOn(service,'getMovieShows').withArgs("tn").and.returnValues(of(data));
spyOn(pipe1,"transform").withArgs("2020-03-20T12:00:00Z","yyyy-MM-DD").and.returnValue("2020-03-20")
component.ngOnInit();
expect(service.getMovieShows).toHaveBeenCalled();
expect(component.control).toBeTruthy();
expect(component.username).toBe("uma");
expect(component.state).toBe("tn");
})
it("should call bookmethod and navigate to booking page",()=>{
 component.book("2020-04-13T12:00:00Z","2020-04-20T12:00:00Z","theri","morning-show","10:00:00","tamil","action",120,1,1);
 expect(mockRouter.navigate).toHaveBeenCalledWith(['/tn/booking/add'], { replaceUrl: true });

})
});
