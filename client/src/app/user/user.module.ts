import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule } from '@angular/forms';

import { UserRoutingModule } from './user-routing.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { DisplayStatesComponent } from './display-states/display-states.component';
import { HomeComponent } from './home/home.component';
import { SearchPipe } from './pipes/search.pipe';
import { BookingsComponent } from './bookings/bookings.component';
import { DatePipe } from '@angular/common';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from "@angular/common/http";

export function httpTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http);
}


@NgModule({
  declarations: [LoginComponent, RegisterComponent, DisplayStatesComponent, HomeComponent, SearchPipe, BookingsComponent,],
  imports: [
    CommonModule,
    UserRoutingModule,
    AngularFontAwesomeModule,
     FormsModule,
     TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpTranslateLoader,
        deps: [HttpClient]
      }
    })

     ],
  providers: [DatePipe]
})
export class UserModule { }
