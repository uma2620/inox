import { getTestBed,async, ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import{ Observable} from 'rxjs';


import { throwError } from 'rxjs';

import { RegisterComponent } from './register.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule } from '@angular/forms';
import { UserService } from '.././shared/user.service';
import { AuthService } from '../../auth/auth.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { By } from "@angular/platform-browser";

// export class mockUserService
// {
//   createUser(state: any, name:string,email: String, password: String):Observable<any[]> {
//     return of([{ success:true,message:"Register Sucessfull"}])
//   }}


fdescribe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let router = {
    navigate: jasmine.createSpy('navigate')
  }
  let service:UserService;
  let injector:TestBed;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterComponent ],
      imports:[AngularFontAwesomeModule,FormsModule,HttpClientModule,HttpClientTestingModule],
      providers:[  AuthService,
        { provide: Router, useValue: router },

        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get: () => "tn", // represents the state
              },
            },
          },
        }, UserService
  ]

    })
    .compileComponents();
    

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
        injector = getTestBed();
    service = injector.get(UserService);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it("state should equal to 'tn'",()=>{

    expect(component.state).toEqual('tn');
  }
  )
  it('should navigate to Home page',async()=>{
    // spyOn(component,'register');
    fixture.detectChanges();
    component.goHome();
    expect(router.navigate).toHaveBeenCalledWith(['/tn/user/home'], { replaceUrl: true });
  })
  it('should navigate to login page',async()=>{
    fixture.detectChanges();
    component.login();
    expect(router.navigate).toHaveBeenCalledWith(['/tn/user/login'],{replaceUrl:true});
  })
  it('should have title register',()=>{
    const title = fixture.debugElement.query(By.css('h3')).nativeElement;
    expect(title.innerHTML).toBe('Register');

  })
  it('should show a error message',async()=>{
    component.control=true;
    component.errorMessage="Error Occured";
    fixture.detectChanges();
    const err = fixture.debugElement.query(By.css('.errorMessage'));
    expect(err.nativeElement.innerHTML).toContain("Error Occured");
})
it('should not show a error message when login sucess',async()=>{
    component.control=false;
    fixture.detectChanges();
    const err = fixture.debugElement.query(By.css('.errorMessage'));
    expect(err).toBeFalsy();
})

  it('should call the register sucessfully',async()=>{
    spyOn(service,'createUser').withArgs("tn","uma","uma@gmail.com","umavirat").and.returnValues(of([{success:true,message:"sucessfully register"}]));

    const data={
      userName:"uma",
      emailId:"uma@gmail.com",
      password:"umavirat"
    }
    component.submit(data);
    // service=TestBed.get('UserService');
    // spyOn(service, 'createUser').withArgs('state','name','email','password').and.returnValue(of([{ success:true,message:"Register Sucessfull"}]));

  //  expect(service.createUser).toHaveBeenCalled();
  // expect(router.navigate).toHaveBeenCalledWith(['/tn/user/login'], { replaceUrl: true });
  expect(service.createUser).toHaveBeenCalled();
   expect(router.navigate).toHaveBeenCalledWith(['/tn/user/login'], { replaceUrl: true });
  })
  it('should call the register and return error',async()=>{
    const error={
      sucess:false,
      message:"register failed",
      error:[
        {
         error:"email Id already exists" 
        }
      ]
  }

    spyOn(service,'createUser').withArgs("tn","uma","uma@gmail.com","umavirat").and.returnValues(throwError(error));

    const data={
      userName:"uma",
      emailId:"uma@gmail.com",
      password:"umavirat"
    }
    component.submit(data);
    // service=TestBed.get('UserService');
    // spyOn(service, 'createUser').withArgs('state','name','email','password').and.returnValue(of([{ success:true,message:"Register Sucessfull"}]));

  //  expect(service.createUser).toHaveBeenCalled();
  // expect(router.navigate).toHaveBeenCalledWith(['/tn/user/login'], { replaceUrl: true });
expect(service.createUser).toHaveBeenCalled();
expect(component.control).toBeTruthy();

  })

});
