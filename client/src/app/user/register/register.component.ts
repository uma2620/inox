import { Component, OnInit } from '@angular/core';
import { UserService } from '.././shared/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import {User} from '.././shared/user';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  control: boolean = false;
  // errorMessage: string;
  user:User=new User();
  errorStatus:boolean=false;
   state: string;
  constructor(private userService: UserService, private route: ActivatedRoute, private router: Router,public translate:TranslateService) {
    translate.setDefaultLang('en');
   }

  ngOnInit() {
    this.state = this.route.snapshot.paramMap.get('state');
  }

  submit() {
    this.userService.createUser(this.state, this.user).subscribe(response => {
      this.router.navigate([`/${this.state}/user/login`]);
    },
      error => {
        this.errorStatus=true;
        // this.errorMessage = error['error']['error']
      });
  }
  login() {
    this.router.navigate([`/${this.state}/user/login`]);
  }
  goHome() {
    this.router.navigate([`/${this.state}/user/home`]);
  }

}
