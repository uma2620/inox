import { Component, OnInit } from '@angular/core';
import { UserService } from '.././shared/user.service';
import { Router } from '@angular/router';
import { AuthService } from '.././../auth/auth.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-display-states',
  templateUrl: './display-states.component.html',
  styleUrls: ['./display-states.component.css']
})
export class DisplayStatesComponent implements OnInit {
  // errorMessage: string;
  states: any[];
  errorStatus: boolean = false;
  selectedState:string='';
  constructor(private authservice: AuthService, private stateService: UserService, private router: Router,public translate:TranslateService) {
    translate.setDefaultLang('en');

   }

  ngOnInit() {
    this.authservice.logout();
    this.stateService.getStates()
      .subscribe((data: any[]) => {
        this.states = data;
      })
  }
  goHome() {
    if (this.selectedState=='') {
      this.errorStatus = true;
      // this.errorMessage = "please select a state to continue"
    }
    else {
      this.router.navigate([`/${this.selectedState}/user/home`]);
    }
  }
  onChange(){
    this.errorStatus=false;
  }
}
