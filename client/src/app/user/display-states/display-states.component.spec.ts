import { By } from "@angular/platform-browser";
import { of } from 'rxjs';

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { UserService } from '.././shared/user.service';
import {  Router ,ActivatedRoute} from '@angular/router';
import { AuthService } from '.././../auth/auth.service';
import {HomeComponent} from '../home/home.component';
import { RouterTestingModule } from '@angular/router/testing';
import {FormsModule} from '@angular/forms';
import {SearchPipe}from '../pipes/search.pipe';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';

import { DisplayStatesComponent } from './display-states.component';
const dummyStateListResponse = {
  success:true,
  message:"states fetched sucessfully",
  results: [
  {id:0,states:"tn"},
  {id:1,states:"bihar"},
  {id:2,states:"andra"}  
  ],
};

fdescribe('DisplayStatesComponent', () => {
  let component: DisplayStatesComponent;
  let fixture: ComponentFixture<DisplayStatesComponent>;
  const userServiceSpy = jasmine.createSpyObj('UserService', {
    getStates: of([{ id:1,states: "tn" },
    {id:2,states:"andra"}
   ]),
});

  let mockRouter;
  let router:Router;
  const dummyStates={
states:[
  { id:1,states: "tn" },
  {id:2,states:"andra"}

]
  }

  beforeEach(async(() => {
    mockRouter = { navigate: jasmine.createSpy('navigate') };
    const MockService = {
      getStates: of([{ id:1,states: "tn" },
                     {id:2,states:"andra"}
                    ]),
    };
  
    TestBed.configureTestingModule({
      declarations: [ DisplayStatesComponent,HomeComponent,SearchPipe ],
      imports:[      FormsModule, HttpClientModule],
      providers:[AuthService,
        {provide:UserService,useValue:userServiceSpy},
        { provide: Router, useValue: mockRouter },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get: () => "tn", // represents the state
              },
            },
          },
        },

      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayStatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('control and error message sould be false',()=>{
    expect(component.control).toBeFalsy();
    const err = fixture.debugElement.query(By.css('.errorMessage'));
    expect(err).toBeFalsy();
  });
  it("should get a states",async()=>{
    expect(component.states).toEqual(dummyStates.states);
  })
  it('should show a error message',async()=>{
    component.control=true;
    component.errorMessage="Error Occured";
    fixture.detectChanges();
    const err = fixture.debugElement.query(By.css('.errorMessage'));
    expect(err.nativeElement.innerHTML).toContain("Error Occured");
})
it('should go to home page when the state is selected',async()=>{
 const state="tn";
 fixture.detectChanges();
  component.goHome(state);
 expect(mockRouter.navigate).toHaveBeenCalledWith(['/tn/user/home'], { replaceUrl: true });
})
it('should show a error message when state is not selected',async()=>{
  const state="";
  fixture.detectChanges();
  component.goHome(state);
  expect(component.control).toBeTruthy();
  expect(component.errorMessage).toBe("please select a state");
  })
  it('should call getStates()  ', function () {
    const service = TestBed.get(UserService); 
    expect(service.getStates).toHaveBeenCalledWith();
  });

});
