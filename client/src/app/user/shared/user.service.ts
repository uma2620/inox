import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { AuthService } from '../../auth/auth.service';
import { User } from './user';
@Injectable({
  providedIn: 'root'
})

export class UserService {
  header = { headers: new HttpHeaders({ 'NoAuth': 'True' }) };
  userId: number;
  constructor(private http: HttpClient, private authservice: AuthService) { }
  getStates() {
    return this.http.get(environment.apiUrl);
  }
   createUser(state: string,user:User) {
    return this.http.post(`${environment.apiUrl}/${state}/user`, user)
  }
  login(state: any, email: String, password: String) {
    const user: any = { email: email, password: password };
    return this.http.post(`${environment.apiUrl}/${state}/user/login`, user, this.header)
  }
  getMovieShows(state) {
    return this.http.get(`${environment.apiUrl}/${state}/movie-shows`)
  }
  getBookings(state) {
    this.userId = this.authservice.getUserId();
    return this.http.get(`${environment.apiUrl}/${state}/booking`)
  }
}
