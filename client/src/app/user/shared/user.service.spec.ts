import { TestBed ,getTestBed} from '@angular/core/testing';

import { UserService } from './user.service';
import { HttpClientTestingModule,HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import {environment} from '../../../environments/environment';
const dummyStateListResponse = {
  success:true,
  message:"states fetched sucessfully",
  results: [
  {id:0,states:"tn"},
  {id:1,states:"bihar"},
  {id:2,states:"andra"}  
  ],
};

const dummyMovieShows={
  success:true,
  message:"movie shows fetched sucessfully",
  results:[
    {
     movie_showsid:1,
     movieid:1,
     moviename:"theri",
     actor:"vijay",
     musicdirector:"gvp",
     director:"atlee",
     language:"tamil",
     genre:"action",
     showname:"morning-show",
     showid:1,
     showtiming:"10:30:00",
     showdate:"13/4/2020"    
    },
    {
      movie_showsid:2,
      movieid:1,
      moviename:"theri",
      actor:"vijay",
      musicdirector:"gvp",
      director:"atlee",
      language:"tamil",
      genre:"action",
      showname:"night-show",
      showid:3,
      showtiming:"11:30:00",
      showdate:"13/4/2020"    
     }
   ]
}

const dummyBookings={
   success:true,
   message:"bookings fetched sucessfully",
   results:[
   {
    booking_id:1,
    userid:1,
    movieid:1,
    moviename:"theri",
    showid:1,
    showdate:"13/4/2020",
    row_seatsid:1,
    rownumber:1,
    seat_number:1
   },
   {
    booking_id:2,
    userid:2,
    movieid:1,
    moviename:"theri",
    showid:1,
    showdate:"13/4/2020",
    row_seatsid:2,
    rownumber:1,
    seat_number:2
   }
  ]
}
export interface RegisterResponse {
  success: boolean,
  message: string
}

export interface loginResponse{
   success:boolean,
   message:string,
   token:string

}

fdescribe('UserService', () => {
  let mockRouter;
  let service: UserService;
  let httpMock: HttpTestingController;


  beforeEach(() => {
  mockRouter = { navigate: jasmine.createSpy('navigate') };


  TestBed.configureTestingModule({
    imports:[HttpClientTestingModule,HttpClientModule],
    providers:[UserService,
      { provide: Router, useValue: mockRouter },

      {
      provide: ActivatedRoute,
      useValue: {
        snapshot: {
          paramMap: {
            get: () => "tn", // represents the state
          },
        },
      },
    }]
  })
  service = TestBed.get(UserService);
  httpMock = TestBed.get(HttpTestingController);

});
afterEach(() => {
  httpMock.verify();
});
it('should be created', () => {
    
  expect(service).toBeTruthy();
});

describe('#registerUser tests', () => {
  it('Should return registerResponse by making a POST request to the given url', () => {

    const username = 'uma';
    const email = 'uma@gmail.com';
    const password = 'umavirat';
    const state='tn';

    const mockResponse: RegisterResponse = {
      success: true,
      message: 'Successfull',
    };
    service.createUser(state,username, email,password).subscribe((res) => {
      expect(res).toBe(mockResponse);
    });

    const req = httpMock.expectOne(`${environment.apiUrl}/${state}/user`);
    expect(req.request.method).toBe('POST');
    req.flush(mockResponse);

  });
});
describe('#Login tests', () => {
  it('Should return a token when login sucessfull', () => {

    const email = 'uma@gmail.com';
    const password = 'umavirat';
    const state='tn';

    const mockResponse: loginResponse = {
      success: true,
      message: 'Successfull',
      token:"eyJpc3MiOiJ0b3B0YWwuY29tIiwiZXhwIjoxNDI2NDIwODAwLCJodHRwOi8vdG9wdGFsLmNvbS9qd3RfY2xhaW1zL2lzX2FkbWluI"
    };
    service.login(state,email,password).subscribe((res) => {
      expect(res).toBe(mockResponse);
    });

    const req = httpMock.expectOne(`${environment.apiUrl}/${state}/user/login`);
    expect(req.request.method).toBe('POST');
    req.flush(mockResponse);

  });
});

describe('#getStates Test',()=>{

   it('getStates() should return list of states', () => {

     service.getStates().subscribe((res) => {

       expect(res).toEqual(dummyStateListResponse);
     });
     const req = httpMock.expectOne(environment.apiUrl);
     expect(req.request.method).toBe('GET');
     req.flush(dummyStateListResponse);
     })
});
describe('#getMovieShows Test',()=>{

  it('should return list of movie-shows', () => {
  
    const state='tn';
    service.getMovieShows(state).subscribe((res) => {

      expect(res).toEqual(dummyMovieShows);
    });
    const req = httpMock.expectOne(`${environment.apiUrl}/${state}/movie-shows`);
    expect(req.request.method).toBe('GET');
    req.flush(dummyMovieShows);
   })
});
xdescribe('#getBookings Test',()=>{

  it('should return list of bookings', () => {
  
    const state='tn';
    service.getBookings(state).subscribe((res) => {

      expect(res).toEqual(dummyBookings);
    });
    const req = httpMock.expectOne(`${environment.apiUrl}/${state}/booking`);
    expect(req.request.method).toBe('GET');
    req.flush(dummyBookings);
   })
});


});