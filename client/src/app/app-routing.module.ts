import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { AdminGuard } from './auth/admin.guard';


const routes: Routes = [
  {
    path: 'admin/state',
    loadChildren: () => import(`./state/state.module`).then(m => m.StateModule)
    , canActivate: [AuthGuard, AdminGuard]
  },
  {
    path: ':state/user',
    loadChildren: () => import(`./user/user.module`).then(m => m.UserModule)
  },
  {
    path: '',
    loadChildren: () => import(`./user/user.module`).then(m => m.UserModule)
  },
  {
    path: ':state/show',
    loadChildren: () => import(`./show/show.module`).then(m => m.ShowModule),
    canActivate: [AuthGuard, AdminGuard]
  },
  {
    path: ':state/movie',
    loadChildren: () => import(`./movie/movie.module`).then(m => m.MovieModule),
    canActivate: [AuthGuard, AdminGuard]
  },
  {
    path: ':state/movie-show',
    loadChildren: () => import(`./movieshow/movieshow.module`).then(m => m.MovieshowModule),
    canActivate: [AuthGuard, AdminGuard]
  },
  {
    path: ':state/booking',
    loadChildren: () => import(`./booking/booking.module`).then(m => m.BookingModule),
    canActivate: [AuthGuard]
  },
  {
    path: ':state/seats',
    loadChildren: () => import(`./seats/seats.module`).then(m => m.SeatsModule),
    canActivate: [AuthGuard, AdminGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
