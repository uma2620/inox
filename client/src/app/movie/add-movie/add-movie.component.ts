import { Component, OnInit } from '@angular/core';
import { MovieService } from '../shared/movie.service';
import { ActivatedRoute, Router } from '@angular/router';
import {Movie}from '../shared/movie';
@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.css']
})
export class AddMovieComponent implements OnInit {
  movie:Movie=new Movie();
  Languages: string[] = [
    'tamil',
    'english',
    'hindi',
    'telungu',
    'malayalam'
  ];
  Genres: string[] = [
    'action',
    'comedy',
    'romance'
  ];
  state: string;
  constructor(private service: MovieService, private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.state = this.route.snapshot.paramMap.get('state');
  }
  submit() {
    this.service.addMovie(this.state, this.movie).subscribe(response => {
      this.router.navigate([`/${this.state}/movie`]);

      console.log(response)
    },
      error => {
        console.log(error)
      });;
  }
  back() {
    this.router.navigate([`/${this.state}/movie`]);
  }
}
