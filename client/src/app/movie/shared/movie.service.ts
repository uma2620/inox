import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private http: HttpClient) { }
  addMovie(state, movie) {
    return this.http.post(`${environment.apiUrl}/${state}/movie`, movie)
  }
  updateMovie(state, movie, id) {
    return this.http.put(`${environment.apiUrl}/${state}/movie/${id}`, movie)
  }
  getMovies(state) {

    return this.http.get(`${environment.apiUrl}/${state}/movie`)
  }
  deleteMovie(state, id) {
    return this.http.delete(`${environment.apiUrl}/${state}/movie/${id}`)
  }

  editMovie(state, id) {
    return this.http.get(`${environment.apiUrl}/${state}/movie/${id}`);
  }

}
