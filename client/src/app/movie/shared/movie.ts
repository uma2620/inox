export class Movie {
    movieName:string;
    language:string;
    genre:string;
    actor:string;
    fare:number;
    director:string;
    musicDirector:string;
}
