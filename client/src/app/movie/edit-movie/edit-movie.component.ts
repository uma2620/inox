import { Component, OnInit } from '@angular/core';
import { MovieService } from '../shared/movie.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Movie } from '../shared/movie';
@Component({
  selector: 'app-edit-movie',
  templateUrl: './edit-movie.component.html',
  styleUrls: ['./edit-movie.component.css']
})
export class EditMovieComponent implements OnInit {
  state: string;
  Movie: any[];
  movie:Movie=new Movie();
  Languages: string[] = [
    'tamil',
    'english',
    'hindi',
    'telungu',
    'malayalam'
  ];
  Genres: string[] = [
    'action',
    'comedy',
    'romance'
  ];
  constructor(private service: MovieService, private route: ActivatedRoute,private router: Router) { }
  ngOnInit() {
    this.state = this.route.snapshot.paramMap.get('state');
    this.route.params.subscribe(params => {
      this.service.editMovie(this.state, params['id']).subscribe(data => {
        this.Movie = data['results'][0];
        this.movie.movieName = this.Movie['moviename'];
        this.movie.language = this.Movie['language'];
        this.movie.fare = this.Movie['fare'];
        this.movie.actor = this.Movie['actor'];
        this.movie.director = this.Movie['director'];
        this.movie.genre = this.Movie['genre'];
        this.movie.musicDirector = this.Movie['musicdirector'];
      })
    });
  }
  update() {
    this.route.params.subscribe(params => {
      this.service.updateMovie(this.state, this.movie, params['id']).subscribe(response => {
        this.router.navigate([`/${this.state}/movie`]);
      },
        error => {
          console.log(error)
        });;
    });
  }
  back() {
    this.router.navigate([`/${this.state}/movie`]);
  }
}
