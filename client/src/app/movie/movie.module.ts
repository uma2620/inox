import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MovieRoutingModule } from './movie-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthInterceptor } from "../auth/auth-interceptor";
import { ListMoviesComponent } from './list-movies/list-movies.component';
import { AddMovieComponent } from './add-movie/add-movie.component';
import { EditMovieComponent } from './edit-movie/edit-movie.component'; 


@NgModule({
  declarations: [ListMoviesComponent, AddMovieComponent, EditMovieComponent],
  imports: [
    CommonModule,
    MovieRoutingModule,
    HttpClientModule,
    FormsModule
  ], providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
})
export class MovieModule { }
