import { Component, OnInit } from '@angular/core';
import { MovieService } from '../shared/movie.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-list-movies',
  templateUrl: './list-movies.component.html',
  styleUrls: ['./list-movies.component.css']
})
export class ListMoviesComponent implements OnInit {
  state: string;
  movies: any[]=[];
  constructor(private service: MovieService, private route: ActivatedRoute,
    private router: Router) { }
  ngOnInit() {
    this.state = this.route.snapshot.paramMap.get('state');

    this.service.getMovies(this.state).subscribe(
      response => {
        this.movies = response['results'];
      },
      error => {
        console.log(error)
      }
    );
  }
  delete(id) {
    this.service.deleteMovie(this.state, id).subscribe(response => {
      this.router.navigate([`/${this.state}/movie`]);
    },
      error => {
        console.log(error)
      });;
  }

  addMovie() {
    this.router.navigate([`/${this.state}/movie/add`]);
  }
  back() {
    this.router.navigate([`/admin/state`]);
  }

}
