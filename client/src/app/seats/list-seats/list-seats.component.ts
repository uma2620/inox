import { Component, OnInit } from '@angular/core';
import { SeatService } from '../shared/seat.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-list-seats',
  templateUrl: './list-seats.component.html',
  styleUrls: ['./list-seats.component.css']
})
export class ListSeatsComponent implements OnInit {
  state: string;
  seats: any[]=[];
  constructor(private seatservice: SeatService, private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.state = this.route.snapshot.paramMap.get('state');

    this.seatservice.getSeats(this.state).subscribe(
      response => {
        this.seats = response['results'];
      },
      error => {
        console.log(error)
      }
    );
  }
  back() {
    this.router.navigate([`/${this.state}/seats`]);
  }
}
