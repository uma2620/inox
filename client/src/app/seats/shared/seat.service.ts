import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class SeatService {
  data: any = {
    seatNumber: Number,
    rowNumber: Number
  };
  constructor(private http: HttpClient) { }
  addseats(state, seatnumber) {
    this.data.seatNumber = seatnumber;
    return this.http.post(`${environment.apiUrl}/${state}/seat`, this.data)

  }
  addrows(state, rownumber) {
    this.data.rowNumber = rownumber;
    console.log(this.data.seatNumber)
    return this.http.post(`${environment.apiUrl}/${state}/row`, this.data)

  }
  addrowseats(state, rownumber, seatnumber) {
    this.data.seatNumber = seatnumber;
    this.data.rowNumber = rownumber;
    return this.http.post(`${environment.apiUrl}/${state}/row-seats`, this.data)

  }
  getSeats(state) {
    return this.http.get(`${environment.apiUrl}/${state}/row-seats`)
  }
}
