import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthInterceptor } from "../auth/auth-interceptor";

import { SeatsRoutingModule } from './seats-routing.module';
import { AddSeatsComponent } from './add-seats/add-seats.component';
import { ListSeatsComponent } from './list-seats/list-seats.component';


@NgModule({
  declarations: [AddSeatsComponent, ListSeatsComponent],
  imports: [
    CommonModule,
    SeatsRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
})
export class SeatsModule { }
