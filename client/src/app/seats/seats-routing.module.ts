import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddSeatsComponent } from './add-seats/add-seats.component';
import { ListSeatsComponent } from './list-seats/list-seats.component';


const routes: Routes = [
  {
    path: '', component: AddSeatsComponent
  },
  {
    path: 'list', component: ListSeatsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SeatsRoutingModule { }
