import { Component, OnInit } from '@angular/core';
import { SeatService } from '../shared/seat.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-seats',
  templateUrl: './add-seats.component.html',
  styleUrls: ['./add-seats.component.css']
})
export class AddSeatsComponent implements OnInit {
  state: string;
  rows:number;
  cols:number;
  constructor(private seatservice: SeatService, private route: ActivatedRoute, private router: Router) { }
  ngOnInit() {
    this.state = this.route.snapshot.paramMap.get('state');
  }
  addSeats() {
    for (let i = 1; i <= this.cols; i++) {
      this.seatservice.addseats(this.state, i).subscribe(
        response => {
          console.log("seat added" + i);
        },
        error => {
          console.log(error)
        }
      );
    }
    for (let i = 1; i <= this.rows; i++) {
      this.seatservice.addrows(this.state, i).subscribe(
        response => {
          console.log("row added" + i);
        },
        error => {
          console.log(error)
        }
      );
    }
    for (let i = 1; i <= this.rows; i++) {
      for (let j = 1; j <= this.cols; j++) {
        this.seatservice.addrowseats(this.state, i, j).subscribe(
          response => {
            console.log("row seats added" + i + j);
          },
          error => {
            console.log(error)
          }
        );
      }
    }
    this.list();
  }
  back() {
    this.router.navigate([`/admin/state`]);
  }
  list() {
    this.router.navigate([`/${this.state}/seats/list`]);
  }
}
